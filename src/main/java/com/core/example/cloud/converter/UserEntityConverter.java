package com.core.example.cloud.converter;

import cn.chinaunicom.sdsi.security.browser.user.UnifastAuthToken;
import com.core.example.cloud.auth.MallUser;
import org.assertj.core.util.Lists;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 自定义用户实体转换类
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2020/11/27
 */
public class UserEntityConverter implements UserAuthenticationConverter {


    private static final String OU = "ou";
    private static final String OU_NAME = "ouName";
    private static final String PROVINCE = "province";
    private static final String CITY = "city";
    private static final String USERNAME = "username";
    private static final String STAFF_ID = "staffId";
    private static final String ORG_ID = "orgId";
    private static final String STAFF_NAME = "staffName";
    private static final String STAFF_ORG_ID = "staffOrgId";
    private static final String STAFF_TYPE = "staffType";
    private static final String TENANT_ID = "tenantId";
    private static final String ENABLED = "enabled";
    private static final String N_A = "N/A";
    private static final String CUSTOM_PARAM = "customParam";
    private static final String AUTHORITY_LIST = "authorityList";
    private static final String USER_JOB_DETAIL_VO_LIST = "userJobDetailVOList";
    private static final String USERID = "userid";
    private static final String PARTNER_ID = "partnerId";
    private static final String PARTNER_NAME = "partnerName";
    private static final String USER_SOURCE = "userSource";
    private static final String PROVINCE_NAME = "provinceName";
    private static final String CITY_NAME = "cityName";
    private static final String IDENTITY_CARD = "identityCard";
    private static final String TEL = "tel";
    private static final String ORG_NAME = "orgName";
    private static final String OU_LIST = "ouList";
    private static final String CUMAIL = "cumail";
    private static final String CURRENT_OU = "currentOu";
    private static final String CURRENT_OU_DISPLAY = "currentOuDisplay";
    private static final String EMPLOYEE_NUMBER = "employeenumber";
    private static final String SITE = "site";
    private static final String MOBILE = "mobile";
    private static final String IS_SUB_COMPANY = "isSubCompany";
    private static final String TYPE = "type";
    private static final String RETURN_URL = "returnUrl";
    private static final String CLIENT_ID = "client_id";
    private static final String EXP = "exp";
    private static final String SCOPE = "scope";
    private static final String ACTIVE = "active";
    private static final String PRINCIPAL_PRO_OU = "principalProOu";
    private static final String PRINCIPAL_CITY_OU = "principalCityOu";
    private static final String PROVINCE_NUM = "provinceNum";
    private static final String CURRENT_ROLE = "currentRole";
    private static final String CUOVER_STATUS = "cuoverStatus";
    private static final String PRINCIPAL_PRO_NAME = "principalProName";
    private static final String PRINCIPAL_CITY_NAME = "principalCityName";
    private static final String DEFAULT_OU = "defaultOu";
    private static final String DEFAULT_NAME = "defaultName";
    private static final String CITY_CODE = "cityCode";
    private static final String IS_GRANSSON = "isGransson";
    public static final String SWITCHOVER = "switchover";
    private static final String IS_SUBSIDIARY = "isSubsidiary";

    /**
     * 客户端认证的map长度
     */
    private static final int CLIENT_MAP_LENGTH = 4;
    public static final String OLD_TOKEN = "oldToken";

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        Map<String, Object> response = new LinkedHashMap<>();
        response.put(USERNAME, authentication.getName());
        if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
            response.put(AUTHORITIES, AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
        }
        final Object principal = authentication.getPrincipal();
        if (principal instanceof MallUser) {
            MallUser entity = (MallUser) principal;
            response.put(USERNAME, entity.getUsername());
            response.put(STAFF_ID, entity.getStaffId());
            response.put(ORG_ID, entity.getOrgId());
            response.put(STAFF_NAME, entity.getStaffName());
            response.put(STAFF_ORG_ID, entity.getStaffOrgId());
            response.put(STAFF_TYPE, entity.getStaffType());
            response.put(ENABLED, entity.isEnabled());
            response.put(OU, entity.getOu());
            response.put(OU_NAME, entity.getOuName());
            response.put(PROVINCE, entity.getProvince());
            response.put(CITY, entity.getCity());
            response.put(TENANT_ID, entity.getTenantId());
            response.put(CUSTOM_PARAM, entity.getCustomParam());
            response.put(AUTHORITY_LIST, entity.getAuthorityList());
            response.put(USER_JOB_DETAIL_VO_LIST, entity.getUserJobDetailVOList());
            response.put(USERID, entity.getUserid());
            response.put(PARTNER_ID, entity.getPartnerId());
            response.put(PARTNER_NAME, entity.getPartnerName());
            response.put(USER_SOURCE, entity.getUserSource());
            response.put(PROVINCE_NAME, entity.getProvinceName());
            response.put(CITY_NAME, entity.getCityName());
            response.put(IDENTITY_CARD, entity.getIdentityCard());
            response.put(TEL, entity.getTel());
            response.put(ORG_NAME, entity.getOrgName());
            response.put(OU_LIST, entity.getOuList());
            response.put(CUMAIL, entity.getCumail());
            response.put(CURRENT_OU, entity.getCurrentOu());
            response.put(CURRENT_OU_DISPLAY, entity.getCurrentOuDisplay());
            response.put(EMPLOYEE_NUMBER, entity.getEmployeenumber());
            response.put(SITE, entity.getSite());
            response.put(MOBILE, entity.getMobile());
            response.put(IS_SUB_COMPANY, entity.getIsSubCompany());
            response.put(TYPE, entity.getType());
            response.put(RETURN_URL, entity.getReturnUrl());
            response.put(CUSTOM_PARAM, entity.getCustomParam());
            response.put(SWITCHOVER, entity.getSwitchover());
            response.put(OLD_TOKEN, entity.getOldToken());
            response.put(PRINCIPAL_PRO_OU, entity.getPrincipalProOu());
            response.put(PRINCIPAL_CITY_OU, entity.getPrincipalCityOu());
            response.put(PROVINCE_NUM, entity.getProvinceNum());
            response.put(CURRENT_ROLE, entity.getCurrentRole());
            response.put(CUOVER_STATUS, entity.getCuoverStatus());
            response.put(PRINCIPAL_PRO_NAME, entity.getPrincipalProName());
            response.put(PRINCIPAL_CITY_NAME, entity.getPrincipalCityName());
            response.put(DEFAULT_OU, entity.getDefaultOu());
            response.put(DEFAULT_NAME, entity.getDefaultName());
            response.put(CITY_CODE, entity.getCityCode());
            response.put(IS_GRANSSON, entity.getIsGransson());
            response.put(IS_SUBSIDIARY, entity.getIsSubsidiary());
        }

        return response;
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        MallUser entity = new MallUser();
        if (map.size() == CLIENT_MAP_LENGTH) {
            // 客户端认证时，map中有4个值
            entity.setClientId(map.get(CLIENT_ID) == null ? "" : String.valueOf(map.get(CLIENT_ID)));
            entity.setExp(map.get(EXP) == null ? 0 : Integer.parseInt(map.get(EXP).toString()));
            entity.setEnabled(map.get(ACTIVE) != null && Boolean.parseBoolean(map.get(ACTIVE).toString()));
            entity.setScope(map.get(SCOPE) == null ? Lists.emptyList() : (List<String>) map.get(SCOPE));
            new UnifastAuthToken(entity, N_A, AuthorityUtils.NO_AUTHORITIES);
        }
        if (map.containsKey(OU)) {
            entity.setOu(String.valueOf(map.get(OU)));
        }
        if (map.containsKey(OU_NAME)) {
            entity.setOuName(String.valueOf(map.get(OU_NAME)));
        }
        if (map.containsKey(PROVINCE)) {
            entity.setProvince(String.valueOf(map.get(PROVINCE)));
        }
        if (map.containsKey(CITY)) {
            entity.setCity(String.valueOf(map.get(CITY)));
        }
        if (map.containsKey(TENANT_ID)) {
            entity.setTenantId(String.valueOf(map.get(TENANT_ID)));
        }
        if (map.containsKey(USERNAME)) {
            entity.setUsername(String.valueOf(map.get(USERNAME)));
        }
        if (map.containsKey(STAFF_ID)) {
            entity.setStaffId(String.valueOf(map.get(STAFF_ID)));
        }
        if (map.containsKey(ORG_ID)) {
            entity.setOrgId(String.valueOf(map.get(ORG_ID)));
        }
        if (map.containsKey(STAFF_NAME)) {
            entity.setStaffName(String.valueOf(map.get(STAFF_NAME)));
        }
        if (map.containsKey(STAFF_ORG_ID)) {
            entity.setStaffOrgId(String.valueOf(map.get(STAFF_ORG_ID)));
        }
        if (map.containsKey(STAFF_TYPE)) {
            entity.setStaffType(String.valueOf(map.get(STAFF_TYPE)));
        }
        if (map.containsKey(SWITCHOVER)) {
            entity.setSwitchover(String.valueOf(String.valueOf(map.get(SWITCHOVER))));
        }
        if (map.containsKey(OLD_TOKEN)) {
            entity.setOldToken(String.valueOf(map.get(OLD_TOKEN)));
        }
        if (map.containsKey(PRINCIPAL_PRO_OU)) {
            entity.setPrincipalProOu(String.valueOf(map.get(PRINCIPAL_PRO_OU)));
        }
        if (map.containsKey(PRINCIPAL_CITY_OU)) {
            entity.setPrincipalCityOu(String.valueOf(map.get(PRINCIPAL_CITY_OU)));
        }
        if (map.containsKey(CUSTOM_PARAM)) {
            Map<String, String> param = (Map<String, String>) map.get(CUSTOM_PARAM);
            if (null != param) {
                entity.getCustomParam().putAll(param);
            }
        }
        if (map.containsKey(AUTHORITY_LIST)) {
            List<LinkedHashMap> value = (List<LinkedHashMap>) map.get(AUTHORITY_LIST);
            if (value != null && !value.isEmpty()) {
                entity.setAuthorityList(value.stream().map(m -> {
                    EshopRoleConverter converter = new EshopRoleConverter();
                    converter.setRoleId(String.valueOf(m.get("roleId")));
                    converter.setRoleCode(String.valueOf(m.get("roleCode")));
                    converter.setRoleName(String.valueOf(m.get("roleName")));
                    converter.setRoleScope(String.valueOf(m.get("roleScope")));
                    converter.setAuthorities((List<String>) m.get("authorities"));
                    converter.setRoleCut(String.valueOf(m.get("roleCut")));
                    return converter;
                }).collect(Collectors.toList()));
            }
//            entity.setAuthorityList((List<EshopRoleConverter>) map.get(AUTHORITY_LIST));
        }
        if (map.containsKey(USER_JOB_DETAIL_VO_LIST)) {
//            entity.setUserJobDetailVOList((List<UserJobDetailConverter>) map.get(USER_JOB_DETAIL_VO_LIST));
            List<LinkedHashMap> value = (List<LinkedHashMap>) map.get(USER_JOB_DETAIL_VO_LIST);
            if (value != null && !value.isEmpty()) {
                entity.setUserJobDetailVOList(value.stream().map(m -> {
                    UserJobDetailConverter converter = new UserJobDetailConverter();
                    converter.setUid(String.valueOf(m.get("uid")));
                    converter.setDisplayname(String.valueOf(m.get("displayname")));
                    converter.setOu(String.valueOf(m.get("ou")));
                    converter.setPassword(String.valueOf(m.get("password")));
                    converter.setIsMaster(String.valueOf(m.get("isMaster")));
                    converter.setOuDisplayname(String.valueOf(m.get("ouDisplayname")));
                    converter.setRealUid(String.valueOf(m.get("realUid")));
                    return converter;
                }).collect(Collectors.toList()));
            }
        }
        if (map.containsKey(USERID)) {
            if (!StringUtils.isEmpty(map.get(USERID))) {
                entity.setUserid(Long.parseLong(String.valueOf(map.get(USERID))));
            } else {
                entity.setUserid(null);
            }
        }
        if (map.containsKey(PARTNER_ID)) {
            entity.setPartnerId(String.valueOf(map.get(PARTNER_ID)));
        }
        if (map.containsKey(PARTNER_NAME)) {
            entity.setPartnerName(String.valueOf(map.get(PARTNER_NAME)));
        }
        if (map.containsKey(USER_SOURCE)) {
            entity.setUserSource(String.valueOf(map.get(USER_SOURCE)));
        }
        if (map.containsKey(PROVINCE_NAME)) {
            entity.setProvinceName(String.valueOf(map.get(PROVINCE_NAME)));
        }
        if (map.containsKey(CITY_NAME)) {
            entity.setCityName(String.valueOf(map.get(CITY_NAME)));
        }
        if (map.containsKey(IDENTITY_CARD)) {
            entity.setIdentityCard(String.valueOf(map.get(IDENTITY_CARD)));
        }
        if (map.containsKey(TEL)) {
            entity.setTel(String.valueOf(map.get(TEL)));
        }
        if (map.containsKey(ORG_NAME)) {
            entity.setOrgName(String.valueOf(map.get(ORG_NAME)));
        }
        if (map.containsKey(OU_LIST)) {
            entity.setOuList((List<String>) map.get(OU_LIST));
        }
        if (map.containsKey(CUMAIL)) {
            entity.setCumail(String.valueOf(map.get(CUMAIL)));
        }
        if (map.containsKey(CURRENT_OU)) {
            entity.setCurrentOu(String.valueOf(map.get(CURRENT_OU)));
        }
        if (map.containsKey(CURRENT_OU_DISPLAY)) {
            entity.setCurrentOuDisplay(String.valueOf(map.get(CURRENT_OU_DISPLAY)));
        }
        if (map.containsKey(EMPLOYEE_NUMBER)) {
            entity.setEmployeenumber(String.valueOf(map.get(EMPLOYEE_NUMBER)));
        }
        if (map.containsKey(SITE)) {
            entity.setSite(String.valueOf(map.get(SITE)));
        }
        if (map.containsKey(MOBILE)) {
            entity.setMobile(String.valueOf(map.get(MOBILE)));
        }
        if (map.containsKey(IS_SUB_COMPANY)) {
            entity.setIsSubCompany(String.valueOf(map.get(IS_SUB_COMPANY)));
        }
        if (map.containsKey(TYPE)) {
            entity.setType(String.valueOf(map.get(TYPE)));
        }
        if (map.containsKey(RETURN_URL)) {
            entity.setReturnUrl(String.valueOf(map.get(RETURN_URL)));
        }
        if (map.containsKey(ENABLED)) {
            entity.setEnabled(Boolean.valueOf(String.valueOf(map.get(ENABLED))));
        }
        if (map.containsKey(PROVINCE_NUM)) {
            entity.setProvinceNum(String.valueOf(map.get(PROVINCE_NUM)));
        }
        if (map.containsKey(CURRENT_ROLE)) {
            entity.setCurrentRole(String.valueOf(map.get(CURRENT_ROLE)));
        }
        if (map.containsKey(CUOVER_STATUS)) {
            entity.setCuoverStatus(String.valueOf(map.get(CUOVER_STATUS)));
        }
        if (map.containsKey(PRINCIPAL_PRO_NAME)) {
            entity.setPrincipalProName(String.valueOf(map.get(PRINCIPAL_PRO_NAME)));
        }
        if (map.containsKey(PRINCIPAL_CITY_NAME)) {
            entity.setPrincipalCityName(String.valueOf(map.get(PRINCIPAL_CITY_NAME)));
        }
        if (map.containsKey(DEFAULT_OU)) {
            entity.setDefaultOu(String.valueOf(map.get(DEFAULT_OU)));
        }
        if (map.containsKey(DEFAULT_NAME)) {
            entity.setDefaultName(String.valueOf(map.get(DEFAULT_NAME)));
        }
        if (map.containsKey(CITY_CODE)) {
            entity.setCityCode(String.valueOf(map.get(CITY_CODE)));
        }
        if (map.containsKey(IS_GRANSSON)) {
            if (!StringUtils.isEmpty(map.get(IS_GRANSSON))) {
                entity.setIsGransson(Integer.valueOf(String.valueOf(map.get(IS_GRANSSON))));
            } else {
                entity.setIsGransson(null);
            }
        }
        if (map.containsKey(IS_SUBSIDIARY)) {
            entity.setIsSubsidiary(String.valueOf(map.get(IS_SUBSIDIARY)));
        }

        entity.setPassword(N_A);
        Collection<? extends GrantedAuthority> authorities = getAuthorities(map);
        entity.setAuthorities(authorities.stream()
                .map(t -> new SimpleGrantedAuthority(t.getAuthority())).collect(Collectors.toList()));
        return new UnifastAuthToken(entity, N_A, authorities);
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {
        if (!map.containsKey(AUTHORITIES)) {
            return AuthorityUtils.NO_AUTHORITIES;
        }
        Object authorities = map.get(AUTHORITIES);
        if (authorities instanceof String) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList((String) authorities);
        }
        if (authorities instanceof Collection) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils
                    .collectionToCommaDelimitedString((Collection<?>) authorities));
        }
        throw new IllegalArgumentException("Authorities must be either a String or a Collection");
    }
}
