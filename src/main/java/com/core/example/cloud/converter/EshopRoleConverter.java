package com.core.example.cloud.converter;/**
 * Created by chentao on 2021/1/6.
 */

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * @author: chentao
 * @time: 2021/1/6 11:21
 */
@Data
public class EshopRoleConverter implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色Id")
    private String roleId;

    @ApiModelProperty(value = "角色编码")
    private String roleCode;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "角色范围")
    private String roleScope;

    @ApiModelProperty(value = "权限字集合")
    private List<String> authorities;

    @ApiModelProperty(value = "是否可切换标识 (Y-是 N-否)")
    private String roleCut;
}
