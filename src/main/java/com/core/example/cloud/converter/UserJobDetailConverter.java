package com.core.example.cloud.converter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author chentao
 * @date 2021-1-15
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */

@Data
@ApiModel(value = "用户岗位信息、权限字信息、角色信息")
public class UserJobDetailConverter implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "门户全国目录中的唯一编码 uid")
    private String uid;

    @ApiModelProperty(value = "般情况下，等于CN字段值，如部门内有重名的情况，可以以特殊名称标明，例如：张明（大），张明（小）等")
    private String displayname;

    @ApiModelProperty(value = "正式用户所属部门名称为长名称，包括公司、部门、处室信息，如中国联通总部管理信息系统部规划应用处，非正式用户则仅包括所属组织信息，如规划应用处")
    private String ouDisplayname;

    @ApiModelProperty(value = "是否主岗，默认0 主岗 1兼职")
    private String isMaster;

    @ApiModelProperty(value = "主岗uid，兼容兼职添加，当为兼职时此值为主岗uid")
    private String realUid;

    @ApiModelProperty(value = "部门编码,根据门户现状，位数不规则，部分沿用HR原编码")
    private String ou;

    @ApiModelProperty(value = "用户密码")
    private String password;

    @ApiModelProperty("是否国际公司组织")
    private Integer isIcompany;

}
