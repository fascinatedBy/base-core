package com.core.example.cloud.token;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.util.AntPathMatcher;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * 自定义token提取器，增加对cookies的读取，自定义参数名
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/2/3
 */
@Slf4j
public class UniTokenExtractor extends BearerTokenExtractor {

    public static final String UNI_TOKEN_NAME = "mall3_token";

    private static final String[] whiteList = {
                    "/v1.0/user/info"
    };

    //private List<String> whiteList;

    public UniTokenExtractor() {
        //默认构造方法
    }

//    public UniTokenExtractor(List<String> whiteList) {
//        //传入白名单的构造方法
//        this.whiteList = whiteList;
//    }

    @Override
    protected String extractToken(HttpServletRequest request) {
        if (whiteList != null) {
            //如果当前路径在白名单中，则不读取token值
            AntPathMatcher pathMatcher = new AntPathMatcher();
            for (String s : whiteList) {
                if (pathMatcher.match(s, request.getRequestURI())) {
                    return null;
                }
            }
        }
        return this.readToken(request);
    }

    /**
     * 从request中获取token
     *
     * @param request HttpServletRequest
     * @return token or null
     */
    public String readToken(HttpServletRequest request) {
        // first check the header...
        String token = extractHeaderToken(request);
        // bearer type allows a request parameter as well
        // and Cookies
        if (token == null) {
            token = readTokenFromCookiesOrParameter(request);
            if (token == null) {
                log.debug("Token not found in request parameters.  Not an OAuth2 request.");
            } else {
                request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE, OAuth2AccessToken.BEARER_TYPE);
            }
        }
        return token;
    }

    /**
     * 从参数或cookies中读取token
     *
     * @param request HttpServletRequest
     * @return String
     */
    public String readTokenFromCookiesOrParameter(HttpServletRequest request) {
        log.debug("Token not found in headers. Trying request parameters.");
        String token = request.getParameter(UniTokenExtractor.UNI_TOKEN_NAME);
        if (token == null) {
            log.debug("Token not found in request parameters.  Trying cookies.");
            final Cookie[] cookies = request.getCookies();
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    if (UNI_TOKEN_NAME.equals(cookie.getName())) {
                        return cookie.getValue();
                    }
                }
            }
        }
        return token;
    }
}
