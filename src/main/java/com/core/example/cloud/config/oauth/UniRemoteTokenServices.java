package com.core.example.cloud.config.oauth;

import com.core.example.cloud.converter.UserEntityConverter;
import com.core.example.framework.utils.RedisUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.stereotype.Component;

/**
 * 自定义资源服务器的ResourceServerTokenServices，第一次取值后，存入本服务的缓存，不再重复远程获取
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2020/11/26
 */
@Slf4j
@Primary
@Component
@ConditionalOnProperty(value = "unifast.cloud.resource.enabled", havingValue = "true", matchIfMissing = true)
public class UniRemoteTokenServices extends RemoteTokenServices {

    @Autowired
    private RedisUtils redisUtils;
    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * 构造方法，设置必要参数
     *
     * @param properties ResourceServerProperties
     */
    public UniRemoteTokenServices(ResourceServerProperties properties) {
        super.setClientId(properties.getClientId());
        super.setClientSecret(properties.getClientSecret());
        super.setCheckTokenEndpointUrl(properties.getTokenInfoUri());
        final DefaultAccessTokenConverter tokenConverter = new DefaultAccessTokenConverter();
        tokenConverter.setUserTokenConverter(new UserEntityConverter());
        super.setAccessTokenConverter(tokenConverter);
    }

    /**
     * 覆盖loadAuthentication方法，增加缓存的读取和写入
     *
     * @param accessToken String
     * @return OAuth2Authentication
     * @throws AuthenticationException AuthenticationException
     * @throws InvalidTokenException   InvalidTokenException
     */
    @Override
    public OAuth2Authentication loadAuthentication(String accessToken) throws AuthenticationException,
            InvalidTokenException {
        Object object = redisUtils.get(accessToken);
        if (object != null) {
            try {
                final UniOauth2Authentication uniOauth2Authentication = mapper.readValue(object.toString(),
                        UniOauth2Authentication.class);
                return uniOauth2Authentication.transfer();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                throw new InvalidTokenException("令牌转换失败");
            }
        }
        try {
            final OAuth2Authentication authentication = super.loadAuthentication(accessToken);
            redisUtils.set(accessToken, mapper.writeValueAsString(new UniOauth2Authentication(authentication)), 3600L);
            return authentication;
        } catch (Exception ex) {
            throw new InvalidTokenException("令牌认证失败");
        }
    }
}
