package com.core.example.cloud.config.oauth;

import com.core.example.cloud.token.UniTokenExtractor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

import java.util.List;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2020/11/24
 */
@Order(88)
@Configuration
@EnableResourceServer
@ConfigurationProperties(prefix = "unifast.cloud.resource")
@ConditionalOnProperty(value = "unifast.cloud.resource.enabled", havingValue = "true", matchIfMissing = true)
public class UniResourceConfig extends ResourceServerConfigurerAdapter {
    @Getter
    @Setter
    private List<String> whiteList;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.stateless(true);
        // 2021-02-03 新增自定义token提取器
        resources.tokenExtractor(new UniTokenExtractor());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // 若干白名单
        if (null != whiteList) {
            for (String au : whiteList) {
                http.authorizeRequests().antMatchers(au).permitAll();
            }
        }
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);
        http.authorizeRequests().anyRequest().authenticated();
    }

}
