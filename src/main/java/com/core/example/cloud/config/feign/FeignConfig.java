package com.core.example.cloud.config.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * 服务调用组件Feign 配置类
 *
 * @author JBY
 */
@Configuration
public class FeignConfig {
    public final static String AUTH_HEAD = "Authorization";

    public final static String TOKEN_HEAD = "Bearer";

    @Bean
    public RequestInterceptor requestTokenBearerInterceptor() {
        return (RequestTemplate requestTemplate) -> {
            HttpServletRequest serRequest = getHttpSerRequest();
            Boolean adminFlag = false;
            String tokenTemp = null;
            if (null != serRequest){
                adminFlag = (Boolean) serRequest.getAttribute("admin_job");
                if (null == adminFlag){
                    adminFlag = (Boolean) serRequest.getAttribute("admin_unify");
                }
                tokenTemp = (String) serRequest.getAttribute("Authorization");
            }
            if ((adminFlag != null && adminFlag)){
                requestTemplate.header(AUTH_HEAD,tokenTemp);
            }else {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (null != authentication) {
                    if (authentication.getDetails() instanceof OAuth2AuthenticationDetails) {
                        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
                        requestTemplate.header(AUTH_HEAD, TOKEN_HEAD + details.getTokenValue());
                    } else if (authentication.getDetails() instanceof WebAuthenticationDetails) {
                        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                        if (null == attributes) {
                            return;
                        }
                        HttpServletRequest request = attributes.getRequest();
                        requestTemplate.header("Access-Token", request.getHeader("X-XSRF-TOKEN"));

                        requestTemplate.header("Cookie", request.getHeader("Cookie"));
                        Cookie cookie = WebUtils.getCookie(request, "XSRF-TOKEN");
                        if (null != cookie){
                            String csrfToken = cookie.getValue();
                            requestTemplate.header("X-XSRF-TOKEN", csrfToken);
                        }
                    }
                }
            }
        };
    }

    private static HttpServletRequest getHttpSerRequest() {
        try {
            return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        } catch (Exception e) {
            return null;
        }
    }

}