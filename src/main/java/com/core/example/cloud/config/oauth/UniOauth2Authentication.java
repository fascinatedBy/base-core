package com.core.example.cloud.config.oauth;

import cn.chinaunicom.sdsi.security.browser.user.UnifastAuthToken;
import com.core.example.cloud.auth.MallUser;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Oauth2Authentication的缓存对象
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2020/12/3
 */
@Data
public class UniOauth2Authentication implements Serializable {

    private static final long serialVersionUID = 1L;

    private String clientId;

    private HashSet<String> resourceIds = Sets.newHashSet();
    private HashSet<String> scope = Sets.newHashSet();

    private ArrayList<SimpleGrantedAuthority> authorities = Lists.newArrayList();

    private MallUser mallUser;

    /**
     * 默认构造方法
     */
    public UniOauth2Authentication() {
        //empty
    }

    /**
     * 根据Oauth2Authentication创建对象
     *
     * @param authentication OAuth2Authentication
     */
    public UniOauth2Authentication(OAuth2Authentication authentication) {
        final OAuth2Request auth2Request = authentication.getOAuth2Request();
        this.clientId = auth2Request.getClientId();
        final Set<String> resourceIds = auth2Request.getResourceIds();
        if (resourceIds != null) {
            this.resourceIds = Sets.newHashSet(resourceIds);
        }
        final Collection<? extends GrantedAuthority> requestAuthorities = auth2Request.getAuthorities();
        if (requestAuthorities != null) {
            this.authorities =
                    requestAuthorities.stream().map(t -> new SimpleGrantedAuthority(t.getAuthority()))
                            .collect(Collectors.toCollection(ArrayList::new));
        }
        final Set<String> scope = auth2Request.getScope();
        if (scope != null) {
            this.scope = Sets.newHashSet(scope);
        }
        final Object principal = authentication.getPrincipal();
        if (principal instanceof MallUser) {
            this.mallUser = (MallUser) principal;
        } else {
            throw new InvalidParameterException("认证信息不匹配");
        }
    }

    public OAuth2Authentication transfer() {
        OAuth2Request request =
                new OAuth2Request(null, this.clientId, this.authorities, true, this.scope,
                        this.resourceIds, null, null, null);
        UnifastAuthToken unifastAuthToken =
                new UnifastAuthToken(this.mallUser, this.mallUser.getPassword(), this.mallUser.getAuthorities());
        return new OAuth2Authentication(request, unifastAuthToken);
    }


}
