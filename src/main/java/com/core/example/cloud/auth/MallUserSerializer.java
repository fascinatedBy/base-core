package com.core.example.cloud.auth;

import com.core.example.cloud.converter.EshopRoleConverter;
import com.core.example.cloud.converter.UserJobDetailConverter;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * MallUser的序列化类
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2020/12/3
 */
public class MallUserSerializer extends JsonDeserializer<MallUser> {
    @Override
    public MallUser deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        MallUser entity = new MallUser();

        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        // 处理每个属性
        if (node.get("staffId") != null){
            entity.setStaffId(node.get("staffId").asText());
        }
        if (node.get("orgId") != null){
            entity.setOrgId(node.get("orgId").asText());
        }
        if (node.get("username") != null){
            entity.setUsername(node.get("username").asText());
        }
        if (node.get("staffOrgId") != null){
            entity.setStaffOrgId(node.get("staffOrgId").asText());
        }
        if (node.get("staffName") != null){
            entity.setStaffName(node.get("staffName").asText());
        }
        if (node.get("password") != null){
            entity.setPassword(node.get("password").asText());
        }
        if (node.get("ouName") != null){
            entity.setOuName(node.get("ouName").asText());
        }
        if (node.get("ou") != null){
            entity.setOu(node.get("ou").asText());
        }
        if (node.get("province") != null){
            entity.setProvince(node.get("province").asText());
        }
        if (node.get("city") != null){
            entity.setCity(node.get("city").asText());
        }
        if (node.get("provinceName") != null){
            entity.setProvinceName(node.get("provinceName").asText());
        }
        if (node.get("cityName") != null){
            entity.setCityName(node.get("cityName").asText());
        }
        if (node.get("enabled") != null){
            entity.setEnabled(node.get("enabled").asBoolean());
        }
        if (node.get("userid") != null){
            entity.setUserid(node.get("userid").asLong());
        }
        if (node.get("partnerId") != null){
            entity.setPartnerId(node.get("partnerId").asText());
        }
        if (node.get("partnerName") != null){
            entity.setPartnerName(node.get("partnerName").asText());
        }

        if(node.get("userSource") != null){
            entity.setUserSource(node.get("userSource").asText());
        }
        if (node.get("identityCard") != null){
            entity.setIdentityCard(node.get("identityCard").asText());
        }
        if (node.get("tel") != null){
            entity.setTel(node.get("tel").asText());
        }
        if (node.get("orgName") != null){
            entity.setOrgName(node.get("orgName").asText());
        }
        if (node.get("cumail") != null){
            entity.setCumail(node.get("cumail").asText());
        }
        if (node.get("currentOu") != null){
            entity.setCurrentOu(node.get("currentOu").asText());
        }
        if (node.get("currentOuDisplay") != null){
            entity.setCurrentOuDisplay(node.get("currentOuDisplay").asText());
        }
        if (node.get("employeenumber") != null){
            entity.setEmployeenumber(node.get("employeenumber").asText());
        }
        if (node.get("site") != null){
            entity.setSite(node.get("site").asText());
        }
        if (node.get("mobile") != null){
            entity.setMobile(node.get("mobile").asText());
        }
        if (node.get("isSubCompany") != null){
            entity.setIsSubCompany(node.get("isSubCompany").asText());
        }
        if (node.get("type") != null){
            entity.setType(node.get("type").asText());
        }
        if (node.get("returnUrl") != null){
            entity.setReturnUrl(node.get("returnUrl").asText());
        }
        if (node.get("switchover") != null){
            entity.setSwitchover(node.get("switchover").asText());
        }
        if (node.get("oldToken") != null){
            entity.setOldToken(node.get("oldToken").asText());
        }
        if (node.get("principalProOu") != null){
            entity.setPrincipalProOu(node.get("principalProOu").asText());
        }
        if (node.get("principalCityOu") != null){
            entity.setPrincipalCityOu(node.get("principalCityOu").asText());
        }
        if (node.get("provinceNum") != null){
            entity.setProvinceNum(node.get("provinceNum").asText());
        }
        if (node.get("currentRole") != null){
            entity.setCurrentRole(node.get("currentRole").asText());
        }
        if (node.get("cuoverStatus") != null){
            entity.setCuoverStatus(node.get("cuoverStatus").asText());
        }
        if (node.get("principalProName") != null){
            entity.setPrincipalProName(node.get("principalProName").asText());
        }
        if (node.get("principalCityName") != null){
            entity.setPrincipalCityName(node.get("principalCityName").asText());
        }

        if (node.get("defaultOu") != null){
            entity.setDefaultOu(node.get("defaultOu").asText());
        }

        if (node.get("defaultName") != null){
            entity.setDefaultName(node.get("defaultName").asText());
        }

        if (node.get("cityCode") != null){
            entity.setCityCode(node.get("cityCode").asText());
        }

        if (node.get("isGransson") != null){
            entity.setIsGransson(node.get("isGransson").asInt());
        }

        if (node.get("isSubsidiary") != null){
            entity.setIsSubsidiary(node.get("isSubsidiary").asText());
        }

        if (node.get("ouList") != null){
            List<String> list = Lists.newArrayList();
            JsonNode ouList = node.get("ouList");
            if (ouList != null){
                if (ouList.isArray())
                {
                    for (final JsonNode objNode : ouList)
                    {
                        list.add(objNode.asText());
                    }
                }
                entity.setOuList(list);
            }
        }

        if (node.get("scope") != null){
            List<String> list = Lists.newArrayList();
            JsonNode scope = node.get("scope");
            if (scope != null){
                if (scope.isArray())
                {
                    for (final JsonNode objNode : scope)
                    {
                        list.add(objNode.asText());
                    }
                }
                entity.setScope(list);
            }
        }

        Iterator<JsonNode> authorityList = node.get("authorityList").elements();
        List<EshopRoleConverter> eshopRoleConverterList = Lists.newArrayList();
        while (authorityList.hasNext()) {
            JsonNode next = authorityList.next();
            ObjectMapper jsonObjectMapper = new ObjectMapper();
            String json = jsonObjectMapper.writeValueAsString(next);
            JSONObject jsonObject = JSONUtil.parseObj(json);
            EshopRoleConverter eshopRoleConverter = JSONUtil.toBean(jsonObject, EshopRoleConverter.class);
            eshopRoleConverterList.add(eshopRoleConverter);

        }
        entity.setAuthorityList(eshopRoleConverterList);

        Iterator<JsonNode> userJobDetailVOList = node.get("userJobDetailVOList").elements();
        List<UserJobDetailConverter> userJobDetailConverterList = Lists.newArrayList();
        while (userJobDetailVOList.hasNext()) {
            JsonNode next = userJobDetailVOList.next();
            ObjectMapper jsonObjectMapper = new ObjectMapper();
            String json = jsonObjectMapper.writeValueAsString(next);
            JSONObject jsonObject = JSONUtil.parseObj(json);
            UserJobDetailConverter userJobDetailConverter = JSONUtil.toBean(jsonObject, UserJobDetailConverter.class);
            userJobDetailConverterList.add(userJobDetailConverter);

        }
        entity.setUserJobDetailVOList(userJobDetailConverterList);

        // 处理权限字
        Iterator<JsonNode> elements = node.get("authorities").elements();
        while (elements.hasNext()) {
            JsonNode next = elements.next();
            JsonNode authority = next.get("authority");
            entity.getAuthorities().add(new SimpleGrantedAuthority(authority.asText()));
        }

        // 处理自定义参数
        final String customParam = node.get("customParam").toString();
        final HashMap<String, String> o = JSONUtil.toBean(customParam, HashMap.class, true);
        entity.getCustomParam().putAll(o);

        return entity;
    }

    private boolean checkNull(Object obj){
        return obj != null;
    }
}
