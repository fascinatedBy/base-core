package com.core.example.cloud.auth;

import cn.chinaunicom.sdsi.security.browser.user.DefaultUserEntity;
import com.core.example.cloud.converter.EshopRoleConverter;
import com.core.example.cloud.converter.UserJobDetailConverter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2020/11/30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonDeserialize(using = MallUserSerializer.class)
public class MallUser extends DefaultUserEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 租户ID
     */
    private String tenantId;
    /**
     * 用户中心组织编码
     */
    private String ou;
    /**
     * 组织名称
     */
    private String ouName;
    /**
     * 用户所在省分ou
     */
    private String province;
    /**
     * 用户所在市分ou
     */
    private String city;

    /**
     * 用户所在省分
     */
    private String provinceName;
    /**
     * 用户所在市分
     */
    private String cityName;

    /**
     * 角色对应的权限字信息
     */
    private List<EshopRoleConverter> authorityList;

    /**
     * 用户岗位信息
     */
    private List<UserJobDetailConverter> userJobDetailVOList;

    /**
     * 用户id
     */
    private Long userid;

    /**
     * 机构id
     */
    private String partnerId;

    /**
     * 机构名称
     */
    private String partnerName;

    @ApiModelProperty("用户来源，作为三个门户标识 ，0 云门户，1 合作方门户，2 专家门户")
    private String userSource;

    @ApiModelProperty("身份证号吗")
    private String identityCard;

    @ApiModelProperty("联系电话")
    private String tel;

    @ApiModelProperty("是否子公司：1-是, 0-否")
    private String isSubCompany;

    @ApiModelProperty("用户类型（省用户:0,集团用户：1,市公司:2）")
    private String type;

    @ApiModelProperty("公司名称")
    private String orgName;

    @ApiModelProperty("当前组织及上级组织信息")
    private List<String> ouList;

    @ApiModelProperty("邮箱")
    private String cumail;

    @ApiModelProperty("当前用户ou")
    private String currentOu;

    @ApiModelProperty("当前用户岗位名称")
    private String currentOuDisplay;

    @ApiModelProperty(value = "HR员工编码（非正式用户无该信息）")
    private String employeenumber;

    @ApiModelProperty("省份简称")
    private String site;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("跳转路径")
    private String returnUrl;

    @ApiModelProperty("客户端id")
    private String clientId;

    @ApiModelProperty("过期时长")
    private Integer exp;

    @ApiModelProperty("作用域")
    private List<String> scope;

    @ApiModelProperty("岗位切换前token")
    private String oldToken;

    @ApiModelProperty("所负责省分")
    private String principalProOu;

    @ApiModelProperty("所负责市分")
    private String principalCityOu;

    @ApiModelProperty("省份code")
    private String provinceNum;

    @ApiModelProperty("当前切换角色")
    private String currentRole;

    @ApiModelProperty("是否是切换岗位登录标识，默认0 ,是 1，否")
    private String switchover;

    @ApiModelProperty("割接状态：1-割接数据，0-新数据 ")
    private String cuoverStatus;

    @ApiModelProperty("所负责省分名称")
    private String principalProName;

    @ApiModelProperty("所负责市分名称")
    private String principalCityName;

    @ApiModelProperty("默认组织ou")
    private String defaultOu;

    @ApiModelProperty("默认组织名称")
    private String defaultName;

    @ApiModelProperty("市分code")
    private String cityCode;

    @ApiModelProperty("是否孙子公司")
    private Integer isGransson;

    @ApiModelProperty("是否有子公司 N 否 Y 是")
    private String isSubsidiary;

}
