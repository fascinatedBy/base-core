package com.core.example.cloud.auth;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2020/12/14
 */
@Data
public class UserMeta implements Serializable {
    /**
     * 角色列表
     */
    private ArrayList<String> roleList = new ArrayList<>();
    /**
     * 权限字列表
     */
    private ArrayList<String> authorities = new ArrayList<>();
}
