package com.core.example.core.config;

import cn.chinaunicom.sdsi.security.util.SpringContextUtil;

import java.util.Collection;
import java.util.Map;

/**
 * @author ct
 * @date EduExcelSelectImpl
 *
 */
public class DictExcelSelectImpl implements ExcelDynamicSelect{
    @Override
    public String[] getSource(Map<String,String> map) {
        DictService bean = SpringContextUtil.getBean(DictService.class);
        Map<String, String> dictMap = bean.dictTranslation(null);
        Collection<String> values = dictMap.values();
        return values.toArray(new String[values.size()]);
    }
}