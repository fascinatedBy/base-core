package com.core.example.core.config;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ct
 * @date
 */
@Slf4j
public class ExcelListener<T> extends AnalysisEventListener<T> {

    private List<T> rows = new ArrayList<>();

    /**
     * object是每一行数据映射的对象
     * @param object
     * @param context
     */
    @Override
    public void invoke(T object, AnalysisContext context) {
        ExcelInValid.valid(object);
        log.info("解析每一行的数据 , 数据内容: {}" , object);
        rows.add(object);
        //根据自己业务做处理
        doSomething(object);
    }

    private void doSomething(T object) {
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

        log.info("所有数据解析完成,数据量:{}",rows.size());
    }

    public List<T> getRows() {

        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

}
