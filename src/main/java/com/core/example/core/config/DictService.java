package com.core.example.core.config;/**
 * Created by chentao on 2022/4/2.
 */

import java.util.Map;

/**
 * @description:
 * @author: chentao
 * @time: 2022/4/2 16:36
 */
public interface DictService {

    Map<String, String> dictTranslation(String key);


}
