package com.core.example.core.config;
 
import lombok.Getter;

/**
 * 批注信息类
 *
 * @author ct
 */
@Getter
public class CommentModel {
    /**
     * sheet页名称
     */
    private String sheetName;
    /**
     * 列索引
     */
    private int colIndex;
    /**
     * 行索引
     */
    private int rowIndex;
    /**
     * 行索引
     */
    private String commentContent;
 
    private void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }
 
    private void setColIndex(int colIndex) {
        this.colIndex = colIndex;
    }
 
    private void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }
 
    private void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }
 
    private CommentModel() {
 
    }
 
    /**
     * 生成批注信息
     *
     * @param sheetName      sheet页名称
     * @param rowIndex       行号
     * @param columnIndex    列号
     * @param commentContent 批注内容
     * @return
     */
    public static CommentModel createCommentModel(String sheetName, int rowIndex, int columnIndex, String commentContent) {
        CommentModel commentModel = new CommentModel();
        //sheet页名称
        commentModel.setSheetName(sheetName);
        //行号
        commentModel.setRowIndex(rowIndex);
        //列号
        commentModel.setColIndex(columnIndex);
        //批注内容
        commentModel.setCommentContent(commentContent);
        return commentModel;
    }
}