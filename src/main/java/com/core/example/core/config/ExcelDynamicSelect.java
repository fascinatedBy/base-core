package com.core.example.core.config;

import java.util.Map;

/**
 * @author ct
 * @date ExcelDynamicSelect
 */
public interface ExcelDynamicSelect {
    /**
     * 获取动态生成的下拉框可选数据
     * @return 动态生成的下拉框可选数据
     */
    String[] getSource(Map<String,String> map);
}