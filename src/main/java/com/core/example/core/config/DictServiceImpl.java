package com.core.example.core.config;/**
 * Created by chentao on 2022/4/2.
 */

import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @description:
 * @author: chentao
 * @time: 2022/4/2 16:37
 */
@Service
public class DictServiceImpl implements DictService{

    @Override
    public Map<String, String> dictTranslation(String key) {
        Map<String, String> map = Maps.newHashMap();
        map.put("k1","是");
        map.put("k2","否");
        return map;
    }

}
