package com.core.example.core.config;

import com.core.example.framework.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * @author ct
 * @date
 */
public class ExcelInValid {
 
    /**
     * Excel导入字段校验
     *
     * @param object 校验的JavaBean 其属性须有自定义注解
     * @author linmaosheng
     */
    public static void valid(Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            //属性的值
            Object fieldValue = null;
            try {
                fieldValue = field.get(object);
            } catch (IllegalAccessException e) {
                throw new BusinessException(field.getAnnotation(ExcelValid.class).message());
            }
 
            //必填校验注解
            boolean isExcelValid = field.isAnnotationPresent(ExcelValid.class);
            if (isExcelValid && Objects.isNull(fieldValue)) {
                throw new BusinessException(field.getAnnotation(ExcelValid.class).message());
            }
            //字符串长度校验注解
            boolean isExcelStrValid = field.isAnnotationPresent(ExcelStrLengthValid.class);
            if (isExcelStrValid) {
                String cellStr = fieldValue.toString();
                int length = field.getAnnotation(ExcelStrLengthValid.class).length();
                if (StringUtils.isNotBlank(cellStr) && cellStr.length() > length) {
                    throw new BusinessException(field.getAnnotation(ExcelStrLengthValid.class).message());
                }
            }
            //int类型校验注解
            boolean isExcelIntValid = field.isAnnotationPresent(ExcelIntValid.class);
            if (isExcelIntValid) {
                if (fieldValue instanceof Integer) {
                    int cellInt = Integer.parseInt(fieldValue.toString());
                    int min = field.getAnnotation(ExcelIntValid.class).min();
                    int max = field.getAnnotation(ExcelIntValid.class).max();
                    if (cellInt < min || cellInt > max) {
                        throw new BusinessException(field.getAnnotation(ExcelIntValid.class).message());
                    }
                }
            }
        }
    }

}