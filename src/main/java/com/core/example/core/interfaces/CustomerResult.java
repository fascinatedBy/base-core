package com.core.example.core.interfaces;

import java.io.Serializable;

/**
 * 自定义返回值接口，各业务系统根据需要自己实现本系统的特定返回值消息
 * 公共消息参考 {@link com.core.example.framework.enums.ResponseEnum}
 *
 * @param <T>
 */
public interface CustomerResult<T extends Serializable> extends Serializable {
    /**
     * 数据
     *
     * @return T
     */
    T getData();

    /**
     * 代码
     *
     * @return String
     */
    String getCode();

    /**
     * 消息
     *
     * @return String
     */
    String getMessages();
}
