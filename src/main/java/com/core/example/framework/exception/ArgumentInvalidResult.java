/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.exception;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @file: com.chinaunicom.sdsi.framework.exception.ArgumentInvalidResult
 * @description:
 * @author: wangwj
 * @date: 2019-02-25
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
@Data
@Accessors(chain = true)
public class ArgumentInvalidResult {
    private String field;
    private Object rejectedValue;
    private String defaultMessage;
}
