/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.exception;

/**
 * 自定义逻辑异常
 *
 * @file: com.chinaunicom.sdsi.framework.exception.LogicException
 * @description:
 * @author: wangwj
 * @date: 2019-02-25
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
public class LogicException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4535634480218340753L;

	public LogicException() {
    }

    public LogicException(String message) {
        super(message);
    }

    public LogicException(String message, Throwable cause) {
        super(message, cause);
    }

    public LogicException(Throwable cause) {
        super(cause);
    }

    public LogicException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
