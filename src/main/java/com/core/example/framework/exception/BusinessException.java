/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.exception;

import com.core.example.framework.enums.ResponseEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 自定义业务异常
 *
 * @file: com.chinaunicom.sdsi.framework.exception.BusinessException
 * @description:
 * @author: wangwj
 * @date: 2019-02-25
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
public class BusinessException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2342152914802857343L;
	/**
     * 业务主键
     */
    @Getter
    @Setter
    private String busiKey;
    /**
     * 状态值枚举对象
     */
    @Setter
    private ResponseEnum respStatus;

    /**
     * 接收枚举对象的构造方法
     *
     * @param responseEnum
     */
    public BusinessException(ResponseEnum responseEnum) {
        this(responseEnum, null);
    }

    /**
     * 接收枚举、业务主键的构造方法
     *
     * @param responseEnum
     * @param busiKey
     */
    public BusinessException(ResponseEnum responseEnum, String busiKey) {
        this.respStatus = responseEnum;
        this.busiKey = busiKey;
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, String busiKey) {
        super(message);
        this.busiKey = busiKey;
    }

    public ResponseEnum getRespStatus() {
        return respStatus;
    }
}