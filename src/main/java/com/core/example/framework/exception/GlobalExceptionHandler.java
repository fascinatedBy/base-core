package com.core.example.framework.exception;

import com.core.example.framework.enums.ResponseEnum;
import com.core.example.framework.response.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 全局异常处理类 此类拦截所有异常，将异常封装到response对象中返回
 * [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 * 1     2020年12月01日     wangwj  增加  AccessDeniedException的简易处理
 *
 * @author wangwj
 * @version V1.0
 * @date 2019-02-25
 */
@Slf4j
@ControllerAdvice
@ResponseBody
@SuppressWarnings({"unchecked", "rawtypes"})
public class GlobalExceptionHandler {

    private static final String ERROR = "error";
    private static final String STATUS = "status";
    private static final String MESSAGE = "message";
    private static final String DETAIL = "detail";
    private static final String PATH = "path";
    /**
     * 错误属性类
     */
    @Autowired
    private ErrorAttributes errorAttributes;

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public BaseResponse<List<ArgumentInvalidResult>> methodArgumentNotValidHandler(HttpServletRequest request,
                                                                                   MethodArgumentNotValidException ex) {
        log.error("GlobalExceptionHandler : Exception when request url :{}", request.getRequestURI());
        log.error("GlobalExceptionHandler : there is a MethodArgumentNotValidException::", ex);
        List<ArgumentInvalidResult> invalidArguments = new ArrayList<>();
        //解析原错误信息，封装后返回，此处返回非法的字段名称，原始值，错误信息
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            ArgumentInvalidResult invalidArgument = new ArgumentInvalidResult();
            invalidArgument.setDefaultMessage(error.getDefaultMessage());
            invalidArgument.setField(error.getField());
            invalidArgument.setRejectedValue(error.getRejectedValue());
            invalidArguments.add(invalidArgument);
        }
        return new BaseResponse(ResponseEnum.STATUS_CODE_102.getCode(), ResponseEnum.STATUS_CODE_102.getMsg(),
                invalidArguments);
    }

    /**
     * 参数绑定异常
     *
     * @param request   请求
     * @param exception 异常对象
     * @return 响应
     */
    @SuppressWarnings("deprecation")
    @ExceptionHandler(value = BindException.class)
    public ResponseEntity<Map<String, Object>> methodArgumentNotValidHandler(
            HttpServletRequest request, BindException exception) {
        log.error("GlobalExceptionHandler : Exception when request url :{}", request.getRequestURI());
        log.error("GlobalExceptionHandler : there is a BindException::", exception);
        //按需重新封装需要返回的错误信息
        List<ArgumentInvalidResult> invalidArguments = new ArrayList<>();
        //解析原错误信息，封装后返回，此处返回非法的字段名称，原始值，错误信息
        for (FieldError error : exception.getBindingResult().getFieldErrors()) {
            ArgumentInvalidResult invalidArgument = new ArgumentInvalidResult();
            invalidArgument.setDefaultMessage(error.getDefaultMessage());
            invalidArgument.setField(error.getField());
            invalidArgument.setRejectedValue(error.getRejectedValue());
            invalidArguments.add(invalidArgument);
        }
        WebRequest webRequest = new ServletWebRequest(request);
        Map<String, Object> body = errorAttributes.getErrorAttributes(webRequest, false);
        body.put(DETAIL, invalidArguments);
        body.put(STATUS, HttpStatus.BAD_REQUEST.value());
        body.put(ERROR, HttpStatus.BAD_REQUEST.getReasonPhrase());
        body.put(PATH, request.getRequestURI());
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    /**
     * 自定义逻辑异常处理
     *
     * @param ex LogicException
     * @return BaseResponse
     */
    @ExceptionHandler(value = {LogicException.class})
    public BaseResponse logicExceptionHandler(HttpServletRequest request, LogicException ex) {
        log.error("GlobalExceptionHandler : Exception when request url :{}", request.getRequestURI());
        log.error("GlobalExceptionHandler : there is a LogicException::", ex);
        return new BaseResponse(ResponseEnum.FAIL.getCode(), ex.getMessage());
    }

    /**
     * 自定义业务异常处理逻辑
     *
     * @param ex BusinessException
     * @return BaseResponse
     */
    @ExceptionHandler(value = {BusinessException.class})
    public BaseResponse businessExceptionHandler(HttpServletRequest request, BusinessException ex) {
        log.error("GlobalExceptionHandler : Exception when request url :{}", request.getRequestURI());
        log.error("GlobalExceptionHandler : there is a BusinessException::", ex);
        String message = ex.getMessage();
        if (null != ex.getRespStatus()) {
            message = ex.getRespStatus().getMsg();
        }
        return new BaseResponse(ex.getRespStatus() == null ? ResponseEnum.FAIL.getCode() :
                ex.getRespStatus().getCode(), message);
    }


    /**
     * 通用异常处理
     *
     * @param ex Exception
     * @return BaseResponse
     */
    @ExceptionHandler(value = {Exception.class})
    public BaseResponse handle(HttpServletRequest request, Exception ex) {
        log.error("GlobalExceptionHandler : Exception when request url :{}", request.getRequestURI());
        log.error("GlobalExceptionHandler : there is a Exception::", ex);
        /*
         * throwable 异常提示
         */
        if (ex instanceof UndeclaredThrowableException) {
            UndeclaredThrowableException throwable = (UndeclaredThrowableException) ex;
            Throwable throwa = throwable.getUndeclaredThrowable();
            if (throwa instanceof BusinessException) {
                BusinessException exception = (BusinessException) throwa;
                return new BaseResponse(exception.getRespStatus().getCode(), exception.getRespStatus().getMsg());
            }
        }
        if (ex instanceof AccessDeniedException) {
            return new BaseResponse(ResponseEnum.STATUS_CODE_403.getCode(), ResponseEnum.STATUS_CODE_403.getMsg());
        }
        if (ex instanceof OAuth2Exception) {
            return new BaseResponse(ResponseEnum.STATUS_CODE_401.getCode(), ResponseEnum.STATUS_CODE_401.getMsg());
        }

        StackTraceElement stackTraceElement= ex.getStackTrace()[0];// 得到异常棧的首个元素
//        System.out.println("File="+stackTraceElement.getFileName());// 打印文件名
//        System.out.println("Line="+stackTraceElement.getLineNumber());// 打印出错行号
//        System.out.println("Method="+stackTraceElement.getMethodName());// 打印出错方法
        if (ObjectUtils.isEmpty(stackTraceElement)|| StringUtils.isEmpty(stackTraceElement.getFileName())||StringUtils.isEmpty(stackTraceElement.getMethodName())){
            return new BaseResponse(ResponseEnum.FAIL.getCode(), ResponseEnum.FAIL.getMsg()+request.getRequestURI()+"请求,"+"处理信息为:"+ex);
        }
        return new BaseResponse(ResponseEnum.FAIL.getCode(), ResponseEnum.FAIL.getMsg()+request.getRequestURI()+"请求,"+"处理信息为:"+ex+",信息位置"+stackTraceElement.getLineNumber()+"行");
//        return new BaseResponse(ResponseEnum.FAIL.getCode(), ResponseEnum.FAIL.getMsg()+request.getRequestURI()+"请求,"+"处理信息为:"+ex+",处理类名为:"+stackTraceElement.getFileName()+",处理方法名为:"+stackTraceElement.getMethodName()+",处理java行号为"+stackTraceElement.getLineNumber());
    }

    /**
     * 请求没有相应处理的异常，常见404
     *
     * @param ex NoHandlerFoundException
     * @return BaseResponse
     */
    @ExceptionHandler(value = {NoHandlerFoundException.class})
    public BaseResponse handle(HttpServletRequest request, NoHandlerFoundException ex) {
        log.error("GlobalExceptionHandler : Exception when request url :{}", request.getRequestURI());
        log.error("GlobalExceptionHandler : there is a NoHandlerFoundException::", ex);
        return new BaseResponse(HttpStatus.NOT_FOUND.value() + "", ex.getMessage());
    }

    /**
     * 请求类型不匹配的异常，常见405
     *
     * @param ex HttpRequestMethodNotSupportedException
     * @return BaseResponse
     */
    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    public BaseResponse handle(HttpServletRequest request, HttpRequestMethodNotSupportedException ex) {
        log.error("GlobalExceptionHandler : Exception when request url :{}", request.getRequestURI());
        log.error("GlobalExceptionHandler : there is a HttpRequestMethodNotSupportedException::", ex);
        return new BaseResponse(HttpStatus.METHOD_NOT_ALLOWED.value() + "", ex.getMessage());
    }

}
