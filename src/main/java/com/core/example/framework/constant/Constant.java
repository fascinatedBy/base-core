package com.core.example.framework.constant;

/**
 * Author: ChePengfei
 * Description:  定义常量
 * Date:2019/4/19
 */
public class Constant {

    private Constant(){
        //this is empty
    }

    public static final String TENANT_PROPERTY_NAME = "tenantId";

    /**
     * 脱敏类型
     * */
    public static final String NAME = "name";
    public static final String CARD = "card";
    public static final String TEL = "tel";
    public static final String PASSPORT = "414";
    public static final String DIPLOMATICPASSPORT = "411";
    public static final String BUSINESSPASSPORT = "412";
    public static final String GENERALBUSINESSPASSPORT = "413";
    public static final String HONGKONGPASSPORT = "420";
    public static final String MACAOPASSPORT = "421";

    public static final String IDCARD = "111";

    /**
     * 添加Log的模块名称和功能名称
     */
    public static final String UPDATE = "update";
    public static final String CREATE = "create";
    public static final String DELETE = "delete";

    public static final String SYS_STAFF_MODULAR = "人员管理";
    public static final String SYS_ORG_MODULAR = "单位管理";

    public static final String SYS_STAFF = "系统用户";


    /**
     * 字典类型
     * */
    /**
     * 综治层级
     * */
    public static final String CM_ORG_LEVEL = "cmOrgLevel";
    /**
     * 社会组织类型
     * */
    public static final String SOCIAL_ORG_TYPE = "societyOrgType";
    /**
     * 关注程度
     * */
    public static final String FOCUS_LEVEL = "focusLevel";
    /**
     * 身份类型
     * */
    public static final String ID_TYPE = "idtype";

    /**
     * 身份类型
     * */
    public static final String CERTIFICATE = "certificateCode";

    /**
     * 身份代码
     * */
    public static final String CERTIFICATE_CODE = "certificateCode";
    /**
     *九小场所类型
     */
    public static final String INDIVIDUAL_PRO = "individualProperty";
    /**
     *事件规模
     */
    public static final String CASE_SIZE = "caseSize";
    /**
     *事件类型
     */
    public static final String CASE_TYPE = "caseType";
    /**
     *事件性质
     */
    public static final String CASE_PROP = "caseProp";
    /**
     * 身份类型
     * */
    public static final String SEX = "sex";
    /**
     * 学历
     * */
    public static final String EDUCATION = "education";
    /**
     * 人员类别
     * */
    public static final String LIT_TYPE = "litType";
    /**
     * 化解方式
     * */
    public static final String DEFUSE = "defuseWay";
    /**
     * 民族
     * */
    public static final String NATION = "nation";

    /**
     * 企业状态
     * */
    public static final String ENTERPRISE_STATUS = "enterpriseStatus";

    /**
     * 群防群治组织类型
     * */
    public static final String MASSORG_TYPE = "massOrgType";

    /**
     * 政治面貌
     * */
    public static final String PARTY = "party";

    /**
     * 职务
     * */
    public static final String JOB = "job";

    /**
     * 专业特长
     * */
    public static final String SPECIALTY = "specialty";

    /**
     * 级别
     * */
    public static final String PERSON_LEVEL = "personLevel";

    /**
     * 专业类别
     * */
    public static final String MAJOR_TYPE = "majorType";

    /**
     * 人员身份
     * */
    public static final String KEEPER_AIDENTITY = "keeperAidentity";

    /**
     * 网格员类型
     * */
    public static final String GRID_TYPE = "gridType";

    /**
     * 网格员层级
     * */
    public static final String GRID_LEVEL = "gridLevel";

    /**
     * 网格分类
     * */
    public static final String GRID_CLAS = "gridClassification";

    /**
     * 特殊网格员分类
     * */
    public static final String SPECIAL_TYPE = "specialType";

    /**
     * 企业规模
     * */
    public static final String ENTERPRISE_SCALE = "enterpriseScale";

    /**
     * 行业分类
     * */
    public static final String INDUSTRY_CATEGORY = "industryCategory";

    /**
     * 经济性质
     * */
    public static final String REGIST_TYPE = "registType";

    /**
     * 发证人类型
     * */
    public static final String ISSUER_TYPE = "issuerType";

    /**
     * 经营范围
     * */
    public static final String ENTERPEISE_SCOPE = "enterpriseScope";

    /**
     * 企业状态
     * */
    public static final String ENTERPEISE_STATUS = "enterpriseStatus";

    /**
     * 企业类型
     * */
    public static final String ENTERPEISE_TYPE = "enterpriseType";


    /**
     * 系统配置参数表  网格员的角色编码
     * */
    public static final String GRID_KEEPER = "gridKeeper";


    /**
     * 标记类型(人类型)
     * */
    public static final String RESIDENT= "resident";


    /**
     * 返回结果
     * */
    public static final String SUCCESS= "success";

    /**
     * 字典值
     * */
    public static final String TRUE= "true";
    /**
     * 字典值
     * */
    public static final String FALSE= "false";
    /**
     * Config表里的
     */
    public static final String ISPOINT_PC= "ispoint_pc";
    public static final String ISPOINT_EXCEL= "ispoint_excel";


    /**
     * 字典值
     * */
    public static final String HAVE = "有";
    public static final String NO_HAVE = "无";
    public static final String YES = "是";
    public static final String NO = "否";
    public static final String ID_CARD = "idcard";
    public static final String PF= "平房";
    public static final String LF= "楼房";
    public static final String CHINACOMMUNIST = "01";

    /**
     * 标记操作类型
     * */
    public static final String OPER_TYPE_ADD = "add";
    public static final String OPER_TYPE_DEL = "delete";
    /**
     *知识库操作类型
     * */
    public static final String OPER_MAG = "manage";
    public static final String OPER_QUERY = "query";
    /**
     * 标绘
     */
    public static final String POINT = "1";
    public static final String CANCEL_POINT = "0";
    /**
     * 标绘删除
     * */
    public static final String OPERATIONTYPES_POINT = "point";
    public static final String OPERATIONTYPES_DELETE= "delete";
    /**
     * 常量
     * */
    public static final String ZERO = "0";
    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final Integer FIVE = 5;
    public static final Integer TEN = 10;
    /**
     * 数据来源
     * */
    public static final String WEB = "web";
    public static final String APP = "app";
    /**
     * 导入导出操作类型
     * */
    public static final String DOWNLODA = "0";
    public static final String EXPORT = "1";
    public static final String IMPORT = "2";

    /**
     *  系统参数里的国标值
     * */
    public static final String APPSCOPEGB = "appScopeWG";
    /***
     * 行政区划根节点编码
     * **/
    public static final String REGION_CODE = "region_root_id";

    /**
     * 删除状态
     */
    public static final String DELETEFLAG="deleted";

    /**
     * 证件类型校验
     * */
    public static final String ID_CARD_CHECK="idcard";
    public static final String PASSPORT_CHECK="passport";

    /**
     * 导入网格类型
     * */
    public static final String SPECIAL_GRID ="special";
    public static final String COMMON_GRID ="common";

    /*网格属性*/
    public static final String VILLAGE ="01";
    public static final String HOUSING_ESTATE ="02";
    public static final String ENTERPRISE ="03";
    public static final String SCHOOL ="04";
    public static final String HOSPITAL ="05";
    public static final String UNIT ="06";
    public static final String OTHER ="07";

    /**
     * 事件消息类型
     * */
    public static final String SYS_MESSAGE ="message";
    public static final String SMS ="sms";
    public static final String EMAIL ="email";

    /**
     * 权限类型
     * */
    public static final String PERMISSION_SETTING_GRIDDATA ="permissionSetting.gridData";
    public static final String PERMISSION_SETTING_BMDATA ="permissionSetting.bmData";
    public static final String PERMISSION_SETTING_EVENTTYPEDATA ="permissionSetting.eventTypeData";

    public static final String SYS_ANNOUNCEMENT = "sys_announcement";

    public static final String OFFICE = "office";

    public static final String REGION ="region";

    public static final String ORG_UNIT = "unit";

    public static final String SIMPLEDATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String UTF_8 = "UTF-8";

    public static final String GBK = "GBK";

}