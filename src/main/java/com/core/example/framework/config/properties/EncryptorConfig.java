package com.core.example.framework.config.properties;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.context.annotation.Bean;

//@Configuration
public class EncryptorConfig {
    /**
     * 默认找jasyptStringEncryptor这个bean，这里直接取这个名字进行注册，省去额外配置
     *
     * @param properties
     * @return
     */

    /**
     * 加解密PBE 算法
     */
    public static final String PBE_ALGORITHMS_MD5_DES = "PBEWITHHMACSHA512ANDAES_256";
    /**
     * 加解密盐值
     */
    private String password = "unicom";

    @Bean("stringEncryptor")
    StringEncryptor jasyptStringEncryptor() {
        return new StringEncryptor() {
            // 加密
            @Override
            public String encrypt(String message) {
                PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
                // 加解密盐值
                encryptor.setConfig(getConfig(password));
                return encryptor.encrypt(message);
            }

            // 解密
            @Override
            public String decrypt(String encryptedMessage) {

                PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
                encryptor.setConfig(getConfig(password));
                return encryptor.decrypt(encryptedMessage);
            }

            public SimpleStringPBEConfig getConfig(String password) {
                SimpleStringPBEConfig config = new SimpleStringPBEConfig();
                // 加密盐值
                config.setPassword(password);
                // 加解密算法
                config.setAlgorithm(PBE_ALGORITHMS_MD5_DES);
                // 设置密钥获取迭代次数
                config.setKeyObtentionIterations(1000);
                // 线程池大小：默认1
                config.setPoolSize(1);
                config.setProviderName("SunJCE");
                // 盐值生成器className
                config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
                //  iv(initialization vector，初始化向量) 生成器className
                config.setIvGeneratorClassName("org.jasypt.iv.RandomIvGenerator");
                // 设置字符串输出类型
                config.setStringOutputType("base64");
                return config;
            }
        };
    }
}

