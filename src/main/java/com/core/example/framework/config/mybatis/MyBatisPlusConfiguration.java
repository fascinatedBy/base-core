package com.core.example.framework.config.mybatis;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis-plus功能配置类
 *
 * @author wangwj
 * @date 2019-01-02
 * @version V1.0
 */
@Configuration
public class MyBatisPlusConfiguration {

    /**
     * 乐观锁插件
     *
     * @return OptimisticLockerInterceptor
     * @author wangwj
     * @date 2019-01-07
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInterceptor();
    }

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

}
