package com.core.example.framework.config.gson;

import com.google.gson.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import springfox.documentation.spring.web.json.Json;

/**
 * Gson配置类
 * https://blog.csdn.net/yongshiaoteman/article/details/100114215
 *
 * @author wangwenjing
 */
@Configuration
public class GsonConfig {

    @Value("${spring.gson.date-format:yyyy-MM-dd HH:mm:ss}")
    private String gsonDateFormat;

    @Bean
    public Gson gson() {
        return new GsonBuilder()
                // 设置日期格式
                .setDateFormat(gsonDateFormat)
                // 处理swagger
                .registerTypeAdapter(Json.class,
                        (JsonSerializer<Json>) (json, type, jsonSerializationContext) ->
                                JsonParser.parseString(json.value()))
                // Long 转 String
                //.setLongSerializationPolicy(LongSerializationPolicy.STRING)
                // 转化null值的属性
                .serializeNulls()
                .create();
    }

    @Bean
    public HttpMessageConverters messageConverters() {
        final GsonHttpMessageConverter messageConverter = new GsonHttpMessageConverter();
        messageConverter.setGson(gson());

        return new HttpMessageConverters(messageConverter);
    }
}
