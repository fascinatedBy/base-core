package com.core.example.framework.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/1/27
 */
@ConfigurationProperties(prefix = cn.chinaunicom.sdsi.core.conf.UnifastProperties.UNIFAST_PREFIX)
public class MallProperties {
    @Getter
    @Setter
    protected String operateLoggerType;

    @Getter
    @Setter
    protected Integer serialNumberLength;
}
