package com.core.example.framework.config.mybatis;

import cn.chinaunicom.sdsi.security.browser.user.DefaultUserEntity;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.core.example.framework.constant.Constant;
import com.core.example.framework.utils.RedisUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


/**
 * 公共属性自动处理类
 *
 * @author wangwj
 * @version V1.0
 * @date 2019-01-03
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    private static final Logger logger = LoggerFactory.getLogger(MyMetaObjectHandler.class);
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 获取人员id
     *
     * @return
     */
    private DefaultUserEntity getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (null == auth) {
            return null;
        }
        // 从principal对象中获取staffId
        if (auth.getPrincipal() instanceof DefaultUserEntity) {
            return (DefaultUserEntity) auth.getPrincipal();
        } else if (auth.getPrincipal() instanceof String) {
            logger.info("----------------{}", auth.getPrincipal());
            // 缓存中获取用户对象
            /*
             * RedisUtils redisUtils =
             * SpringContextUtil.getApplicationContext().getBean(RedisUtils.class);
             * if(redisUtils.exists("currentUser::"+auth.getPrincipal())){ SysStaffVO
             * staffVO = (SysStaffVO) redisUtils.get("currentUser::"+auth.getPrincipal());
             * return staffVO.getStaffOrgId(); }
             */

        }

        return null;

    }


    /**
     * 插入的自动赋值
     *
     * @param metaObject 元数据
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        //查出返回的人员的当前登录的岗位id
        DefaultUserEntity user = getCurrentUser();
        //版本号3.0.6以及之前的版本
        ///this.setFieldValByName("operator", "Jerry", metaObject)
        //this.setFieldValByName("createBy", staffId, metaObject);
        if (user != null) {
            this.setFieldValByName("createBy", user.getStaffId(), metaObject);
            this.setFieldValByName(Constant.TENANT_PROPERTY_NAME, user.getCustomParam().get(Constant.TENANT_PROPERTY_NAME), metaObject);
            // this.setFieldValByName("tenantName", user.getCustomParam().get("tenantName"), metaObject);
        }
        this.setFieldValByName("createDate", LocalDateTime.now(), metaObject);
        this.setFieldValByName("updateDate", LocalDateTime.now(), metaObject);
    }

    /**
     * 更新时的自动赋值
     *
     * @param metaObject 元数据
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        //查出返回的人员的当前登录的岗位id
        DefaultUserEntity user = getCurrentUser();
        if (user != null) {
            this.setFieldValByName("updateBy", user.getStaffId(), metaObject);
        }
        this.setFieldValByName("updateDate", LocalDateTime.now(), metaObject);
    }
}
