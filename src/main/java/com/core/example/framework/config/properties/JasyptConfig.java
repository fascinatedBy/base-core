package com.core.example.framework.config.properties;

import org.jasypt.encryption.StringEncryptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description:
 * @author: chentao
 * @time: 2022/9/23 14:45
 */
@Configuration
public class JasyptConfig {

    /**
     * 加解密盐值
     */
    private String password = "unicom";

    @Bean("stringEncryptor")
    public StringEncryptor jasyptEncryptor() {
        return new JasyptEncryptor(password);
    }

}
