package com.core.example.framework.base;

import com.core.example.framework.response.BasePageResponse;
import com.core.example.framework.response.BaseResponse;
import com.core.example.framework.utils.CoreUserUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.http.HttpStatus;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 基类控制器，继承自Unifast抽象类AbstractController
 * 统一处理应用级别控制器的功能，例如：登陆人信息、返回前端的JSON格式
 *
 * @file: com.chinaunicom.sdsi.framework.base.BaseController
 * @description: 主要功能：1.获取当前登陆人信息 2.封装返回前端的JSON对象
 * @author: lishou
 * @date: 2018-10-12
 * @version: V1.0
 * @copyright: Copyright(c) 2018 Sdcncsi
 */
public class BaseController {

    /**
     * 获取登录用户唯一标识
     *
     * @return 用户名
     */
    protected String getUserName() {
        return CoreUserUtil.getName();
    }

    /**
     * 封装返回前端的分页JSON对象 ： 面向mybatis-plus
     *
     * @param data  mybatis-plus的IPage接口
     * @param <T>   结果对象
     * @param draw  绘画计数器
     * @param error 错误信息
     * @return BasePageResponse
     */
    public <T> BasePageResponse<T> pageError(IPage<T> data, Integer draw, String error) {
        return new BasePageResponse<>(data, draw, error);
    }

    /**
     * 封装返回前端的分页JSON对象 ： 面向mybatis-plus
     *
     * @param data mybatis-plus的IPage接口
     * @param <T>  结果对象
     * @param draw 绘画计数器
     * @return BasePageResponse
     * @author wangwj
     * @date 2019-2-26
     */
    public <T> BasePageResponse<T> pageOk(IPage<T> data, Integer draw) {
        return new BasePageResponse<>(data, draw);
    }

    /**
     * 封装返回前端的分页JSON对象 ： 面向mybatis-plus
     *
     * @param data mybatis-plus的IPage接口
     * @param <T>  结果对象
     * @return BasePageResponse
     * @author sunq
     * @date 2019-2-27
     */
    public <T> BasePageResponse<T> pageOk(IPage<T> data) {
        return new BasePageResponse<>(data, 0);
    }


    /**
     * 正常请求返回数据
     *
     * @param data T
     * @param <T>  <T>
     * @return BaseResponse<T>
     */
    protected <T> BaseResponse<T> ok(T data) {
        return new BaseResponse<>(data);
    }


    @SuppressWarnings({"rawtypes", "unchecked"})
    protected <T> BaseResponse<T> notOk() {
        return new BaseResponse(HttpStatus.MULTI_STATUS);
    }

    /**
     * exception转string
     * @param e Exception
     * @return String
     */
    public String exception2Str(Exception e) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }
}
