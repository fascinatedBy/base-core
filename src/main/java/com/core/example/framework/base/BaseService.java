package com.core.example.framework.base;

/**
 * Service 基础类
 *
 * @author lishou
 * @update [1][2019-1-8][wangwj][增加setPageInfo(BaseQueryVO)方法，实现基于pageHelper的排序功能]
 */
public interface BaseService {

}
