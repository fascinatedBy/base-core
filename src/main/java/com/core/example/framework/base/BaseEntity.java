package com.core.example.framework.base;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 实体基类
 *
 * @file: com.chinaunicom.sdsi.framework.base.BaseEntity
 * @description: 所有对应数据库的实体继承此类，统一处理主键、创建人、创建日期、逻辑删除等字段
 * @author: wangwj
 * @date: 2018-12-25
 * @version: V1.0
 * @copyright: Copyright(c) 2018 Sdcncsi Co. Ltd. All rights reserved.
 */
@Data
@Accessors(chain = true)
public class BaseEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7920715128210690669L;


    /**
     * 非数据库表字段
     */
    @TableField(exist = false)
    private List<Long> idList;


    /**
     * 租户标识
     */
    @ApiModelProperty("租户id")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;

    /**
     * 编辑人
     */
    @ApiModelProperty("编辑人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 编辑时间
     */
    @ApiModelProperty("编辑时间")
    @TableField(fill = FieldFill.INSERT_UPDATE, update = "now()")
    private LocalDateTime updateDate;

    /**
     * 乐观锁
     */
    @Version
    @ApiModelProperty("乐观锁")
    private Long versions;

    /**
     * 逻辑删除，normal表示正常，deleted表示删除
     */
    @TableLogic(value = "normal", delval = "deleted")
    @ApiModelProperty("逻辑删除，normal表示正常，deleted表示删除")
    private String deleteFlag = "normal";
}
