/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @file: cn.chinaunicom.sdsi.framework.base.MyBaseMapper
 * @description:
 * @author: xiaoying
 * @date: 2019-05-22
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
public interface MyBaseMapper<T> extends BaseMapper<T> {


    /**
     * 自定义全局方法
     * @author xiaoying
     * @date 2019-05-24
     * @param entity
     * @return int
     * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
     */
    int deleteByIdWithFill(T entity);

    /**
     * 自定义全局批量删除方法
     * @author xiaoying
     * @date 2019-05-24
     * @param entity
     * @return int
     * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
     */
    int deleteBatchByIdsWithFill(T entity);
}
