/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 排序条件对象
 * @file: com.chinaunicom.sdsi.framework.request.DataTableOrder
 * @description:
 * @author: wangwj
 * @date: 2019-02-25
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
@Data
@ApiModel("排序条件对象")
public class DataTableOrder implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2388816063829279290L;
	@ApiModelProperty("排序的列")
    private String column;
    @ApiModelProperty("排序规则:asc 或 desc")
    private String dir;
}
