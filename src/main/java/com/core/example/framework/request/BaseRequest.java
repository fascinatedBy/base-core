/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @file: com.chinaunicom.sdsi.framework.request.BaseRequest
 * @description:
 * @author: wangwj
 * @date: 2019-02-25
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
@Data
@ApiModel("请求对象基类")
public class BaseRequest implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1740989767502459819L;

	@Setter
    @Getter
    @ApiModelProperty(value = "请求来源")
    private String source;

    @ApiModelProperty(value = "绘画计数器")
    private Integer draw;

    @Setter
    @ApiModelProperty(value = "当前页", hidden = true)
    private Integer pageNum;

    @Setter
    @ApiModelProperty(value = "每页的数量", hidden = true)
    private Integer pageSize;

    @Setter
    @ApiModelProperty(value = "起始记录行数")
    private Integer start;

    @Setter
    @ApiModelProperty(value = "每页的数量")
    private Integer length;

    @ApiModelProperty("排序条件")
    private List<DataTableOrder> order;

    public Integer getPageNum() {
        if (null != start && null != length) {
            this.pageNum = (start / length + 1);
        }
        return this.pageNum;
    }

    public Integer getPageSize() {
        if (null != start && null != length) {
            this.pageSize = length;
        }
        return pageSize;
    }

}
