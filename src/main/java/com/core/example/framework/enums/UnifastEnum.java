package com.core.example.framework.enums;

/**
 * 数据有效无效
 *
 * @author seal
 */
public class UnifastEnum {
    /**
     * 数据有效字段枚举值
     *
     * @author wangwj
     */
    public enum SysDataValid {
        // 有效
        valid("valid"),
        // 无效
        invalid("invalid");

        private String dataValid;

        public String value() {
            return this.dataValid;
        }

        private SysDataValid(String dataValid) {
            this.dataValid = dataValid;
        }
    }

    /**
     * 数据有效字段枚举值
     *
     * @author wangwj
     */
    public enum DeleteFlag {
        // 正常
        normal("normal"),
        // 已删除
        deleted("deleted");

        private String deleteFlag;

        public String value() {
            return this.deleteFlag;
        }

        private DeleteFlag(String deleteFlag) {
            this.deleteFlag = deleteFlag;
        }
    }

    /**
     * 组织类型：公司、部门、虚拟组织
     */
    public enum SysOrgType {
        /**
         * 公司
         */
        org("org"),
        /**
         * 部门
         */
        dept("dept"),
        /**
         * 虚拟组织
         */
        vorg("vorg"),
        /**
         * 街道
         */
        street("street"),
        /**
         * 管区
         */
        area("area"),
        /**
         * 网格
         */
        grid("grid"),
        /**
         * 其他
         */
        other("other");

        public String value() {
            return this.type;
        }

        private String type;

        private SysOrgType(String type) {
            this.type = type;
        }
    }

    /**
     * 权限类型：menu-菜单，operation-操作
     *
     * @author wangwj
     */
    public enum PermissionType {
        // 菜单
        menu("menu"),
        // 操作
        operation("operation");

        private String permissionType;

        public String value() {
            return this.permissionType;
        }

        private PermissionType(String permissionType) {
            this.permissionType = permissionType;
        }
    }

    public enum StaffOrgType {
        /**
         * 主岗
         */
        fulltime("F"),
        /**
         * 兼岗
         */
        parttime("T"),
        /**
         * 借调
         */
        borrow("J");

        private String staffOrgType;

        public String value() {
            return this.staffOrgType;
        }

        private StaffOrgType(String staffOrgType) {
            this.staffOrgType = staffOrgType;
        }
    }
}
