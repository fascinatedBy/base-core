package com.core.example.framework.enums;

/**
 * 系统状态码枚举类
 * 此枚举记录系统所有返回值状态，code不可重复，根据业务情况，合理编排code值
 * [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwj
 * @version V1.0
 * @date 2019-02-25
 */

public enum ResponseEnum {
    /**
     * 请求成功
     */
    SUCCESS("1", "操作成功"),
    /**
     * 系统内部异常
     */

    FAIL("0", "系统内部异常") ,
    /**
     * 系统服务错误
     */
    SYSTEM_SERVICE_ERROR("-2", "系统服务错误，请稍后再试!"),
    /**
     * http 102
     */
    STATUS_CODE_102("102", "请求参数不正确"),
    /**
     * http 200
     */
    STATUS_CODE_200("200", "正确"),
    /**
     * http 401
     */
    STATUS_CODE_401("401", "未授权"),
    /**
     * http 403
     */
    STATUS_CODE_403("403", "禁止访问"),
    /**
     * http 404
     */
    STATUS_CODE_404("404", "目标未找到"),
    /**
     * http 405
     */
    STATUS_CODE_405("405", "请求类型错误"),
    /**
     * http 500
     */
    STATUS_CODE_500("500", "系统异常"),
    /**
     * 自定义状态码 没有查询到数据
     */
    NO_DATA("1001", "没有查询到数据"),
    /**
     * 自定义状态码 参数不能为空
     */
    PARAMETER_NONEMPTY("2001", "参数不能为空"),
    /**
     * 自定义状态码 类型错误
     */
    TYPE_ERROR("3001", "类型错误"),
    /**
     * 自定义状态码 更新数据失败
     */
    UPDATE_DATA_FAILURE("3002", "更新数据失败"),

    /**
     * 自定义状态码 业务处理中,请稍后
     */
    REPEAT_SUBMIT("50001", "业务处理中,请稍后");
    private final String code;
    private final String msg;

    ResponseEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public String getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

//    @Component
//    static class EnumApplicationName {
//
//
//        private static String springApplicationName;
//
//        @Value("${spring.application.chineseName:${spring.application.name}}")
//        public void init(String springApplicationName) {
//            EnumApplicationName.springApplicationName = springApplicationName;
//        }
//    }

}
