package com.core.example.framework.utils;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.google.common.base.Preconditions;
import org.apache.shardingsphere.core.strategy.keygen.TimeService;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Random;

public class SnowFlakeId {


    /**
     * 时间戳 2020-10-01
     */
    public static final long epoch = 1601481600L;

    private static TimeService timeService = new TimeService();
    private Properties properties = new Properties();
    /**
     * 序列重置
     */
    private int sequenceOffset = -1;
    /**
     * 机器id所占的位数
     */
    private final long workerIdBits = 5L;

    /**
     * 数据标识id所占的位数
     */
    private final long dataCenterIdBits = 5L;

    /**
     * 支持的最大机器id，结果是31 (这个移位算法可以很快的计算出几位二进制数所能表示的最大十进制数)
     */
    private final long maxWorkerId = -1L ^ (-1L << workerIdBits);

    /**
     * 支持的最大数据标识id，结果是31
     */
    private final long maxDataCenterId = -1L ^ (-1L << dataCenterIdBits);

    /**
     * 序列在id中占的位数
     */
    private final long sequenceBits = 12L;

    /**
     * 机器ID向左移12位
     */
    private final long workerIdShift = sequenceBits;

    /**
     * 数据标识id向左移17位(12+5)
     */
    private final long datacenterIdShift = sequenceBits + workerIdBits;

    /**
     * 时间截向左移22位(5+5+12)
     */
    private final long timestampLeftShift = sequenceBits + workerIdBits + dataCenterIdBits;

    /**
     * 生成序列的掩码，这里为4095 (0b111111111111=0xfff=4095)
     */
    private final long sequenceMask = -1L ^ (-1L << sequenceBits);

    /**
     * 工作机器ID(0~31)
     */
    private long workerId;

    /**
     * 数据中心ID(0~31)
     */
    private long dataCenterId;

    /**
     * 毫秒内序列(0~4095)
     */
    private long sequence = 0L;

    /**
     * 上次生成ID的时间截
     */
    private long lastMilliseconds = -1L;


    /*
     * @Description  主键生产方法
     * @Date  2020/10/3
     * @Author binz
     * @version 1.0
     */
    public synchronized long generateKey() {
        // 当前时间
        long currentMilliseconds = timeService.getCurrentMillis();
        // 是否在容忍时间内
        if (this.waitTolerateTimeDifferenceIfNeed(currentMilliseconds)) {
            // 重新获取当前时间
            currentMilliseconds = timeService.getCurrentMillis();
        }
        // 同一时间生成的，则进行毫秒内序列
        if (this.lastMilliseconds == currentMilliseconds) {

            sequence = (sequence + 1) & sequenceMask;
            // 毫秒内序列溢出
            if (0L == sequence) {
                // 阻塞到下一个毫秒,获得新的时间戳
                currentMilliseconds = this.waitUntilNextTime(currentMilliseconds);
            }
        } else {
            // 时间戳改变，毫秒内序列重置
            this.vibrateSequenceOffset();
            this.sequence = (long) this.sequenceOffset;
        }
        //上次生成ID的时间截
        this.lastMilliseconds = currentMilliseconds;

        // 移位并通过或运算拼到一起组成64位的ID
        return ((currentMilliseconds - epoch) << timestampLeftShift)
                | (this.getDataCenterId() << datacenterIdShift)
                | (this.getWorkerId() << workerIdShift)
                | sequence;
    }

    /*
     * @Description 时间回摆
     * @Date  2020/10/3
     * @Author binz
     * @version 1.0
     */
    private boolean waitTolerateTimeDifferenceIfNeed(long currentMilliseconds) {
        try {
            // 当前时间大于上一次ID生成的时间戳，
            if (this.lastMilliseconds <= currentMilliseconds) {
                return false;
            } else {
                // 时间差
                long timeDifferenceMilliseconds = this.lastMilliseconds - currentMilliseconds;
                // 时间校验
                Preconditions.checkState(timeDifferenceMilliseconds < (long) this.getMaxTolerateTimeDifferenceMilliseconds(), "Clock is moving backwards, last time is %d milliseconds, current time is %d milliseconds", new Object[]{this.lastMilliseconds, currentMilliseconds});
                // 线程等待
                Thread.sleep(timeDifferenceMilliseconds);

                return true;
            }
        } catch (Throwable var5) {
            var5.getMessage();
        }
        return false;
    }

    /*
     * @MethodName
     * @Description 工作id -> 用服务器ip生成，ip转化为long再模32（如有集群部署，且机器ip是连续的,
       模就能避免重复.如服务器ip不连续,不推荐使用
        数据中心ID -> 取0～31的随机数
     * modified by zhaofc 2021年4月14日17:23:07  https://blog.csdn.net/qq_39135287/article/details/88964572/?utm_term=workerid%E9%9B%AA%E8%8A%B1%E7%AE%97%E6%B3%95&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-1-88964572&spm=3001.4430
     * @Date  2020/10/2
     * @Author binz
     * @version 1.0
     */
    private long getWorkerId() {
        // 获取ip
        String ip = getHostIp();
        // 获取配置
        long result = Long.valueOf(this.properties.getProperty("worker.id", String.valueOf(0L)));
        // ip不为空且未有配置
        if (ip != null && result == 0L) {
            //2021年4月14日17:02:07用友发现有问题，改进生成方法
            /*workerId = Long.parseLong(ip.replaceAll("\\.", ""));
            // 模32
            workerId = workerId % 32;*/
            int[] ints = org.apache.commons.lang3.StringUtils.toCodePoints(ip);
            int sums = 0;
            for(int b : ints){
                sums += b;
            }
            workerId= (long)(sums % 32);
        } else if (result == 0L) {
            Random rd = new Random();
            workerId = rd.nextInt(31);
        }
        Preconditions.checkArgument(workerId >= 0L && workerId < maxWorkerId);
        return workerId;
    }
    private long generateKey(long workid,long dataCenterId){
        Snowflake snowflake = IdUtil.getSnowflake(workid, dataCenterId);
        long id = snowflake.nextId();
        return id;
    }
    private SnowFlakeId() {

    }

    private static class SnowFlakeIdHolder {
        private static final SnowFlakeId INSTANCE = new SnowFlakeId();
    }

    public static final SnowFlakeId getInstance() {

        return SnowFlakeIdHolder.INSTANCE;
    }

    /*
     * @Description 数据中心构造
     * @Date  2020/10/3
     * @Author binz
     * @version 1.0
     */
    private long getDataCenterId() {
        // 获取PID
        RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
        String pid = runtimeMXBean.getName().split("@")[0];
        // 获取配置
        long result = Long.valueOf(this.properties.getProperty("worker.pid", String.valueOf(0L)));
        // PID获取且未配置
        if (pid != null && result == 0L) {

            dataCenterId = pid.hashCode();

            dataCenterId = dataCenterId % 32;

        } else if (result == 0L) {
            // 随机获取
            Random rd = new Random();

            dataCenterId = rd.nextInt(31);
        }

        Preconditions.checkArgument(dataCenterId >= 0L && dataCenterId < maxDataCenterId);

        return dataCenterId;
    }


    private int getMaxVibrationOffset() {
        int result = Integer.parseInt(this.properties.getProperty("max.vibration.offset", String.valueOf(1)));
        Preconditions.checkArgument(result >= 0 && (long) result <= 4095L, "Illegal max vibration offset");
        return result;
    }

    /*
     * @Description  获取容忍等待时长
     * @Date  2020/10/3
     * @Author binz
     * @version 1.0
     */
    private int getMaxTolerateTimeDifferenceMilliseconds() {
        return Integer.valueOf(this.properties.getProperty("max.tolerate.time.difference.milliseconds",
                String.valueOf(10)));
    }

    private long waitUntilNextTime(long lastTime) {

        long result = timeService.getCurrentMillis();
        while (result <= lastTime) {
            result = timeService.getCurrentMillis();
        }
        return result;
    }

    private void vibrateSequenceOffset() {

        this.sequenceOffset = this.sequenceOffset >= this.getMaxVibrationOffset() ? 0 : this.sequenceOffset + 1;
    }


    private static String getHostIp() {
        try {
            Enumeration<NetworkInterface> allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            while (allNetInterfaces.hasMoreElements()) {
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();
                Enumeration<InetAddress> addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress ip = (InetAddress) addresses.nextElement();
                    if (ip != null
                            && ip instanceof Inet4Address
                            && !ip.isLoopbackAddress()
                            && ip.getHostAddress().indexOf(":") == -1) {
                        return ip.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
