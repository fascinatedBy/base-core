package com.core.example.framework.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils extends org.springframework.util.StringUtils {

    private static final Logger logger = LoggerFactory.getLogger(StringUtils.class);

    public StringUtils() {
        //this is empty
    }

    public static int appearNum(String srcText, String findText) {
        int count = 0;

        for (int index = 0; (index = srcText.indexOf(findText, index)) != -1; ++count) {
            index += findText.length();
        }

        return count;
    }

    public static String underscoreName(String camelCaseName) {
        StringBuilder result = new StringBuilder();
        if (camelCaseName != null && camelCaseName.length() > 0) {
            result.append(camelCaseName.substring(0, 1).toLowerCase());

            for (int i = 1; i < camelCaseName.length(); ++i) {
                char ch = camelCaseName.charAt(i);
                if (Character.isUpperCase(ch)) {
                    result.append("_");
                    result.append(Character.toLowerCase(ch));
                } else {
                    result.append(ch);
                }
            }
        }

        return result.toString();
    }

    public static String camelCaseName(String underscoreName) {
        StringBuilder result = new StringBuilder();
        if (underscoreName != null && underscoreName.length() > 0) {
            boolean flag = false;

            for (int i = 0; i < underscoreName.length(); ++i) {
                char ch = underscoreName.charAt(i);
                if ("_".charAt(0) == ch) {
                    flag = true;
                } else if (flag) {
                    result.append(Character.toUpperCase(ch));
                    flag = false;
                } else {
                    result.append(ch);
                }
            }
        }

        return result.toString();
    }

    public static boolean isNotEmpty(Object str) {
        return !isEmpty(str);
    }

    /**
     * 判断是否为汉字
     * @param str
     * @return
     */
    public static Boolean isChineseCharacter(String str) {
        Boolean index = false;
        if (isEmpty(str)){
            return index;
        }
        String regEx = "[\\u4e00-\\u9fa5]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        int count = 0;
        while (m.find()) { //m.matches()全部匹配为true
            //m.groupCount()用于获取正则模式中子模式匹配的组，即只有正则中含有（）分组的情况下才有用
            for (int i = 0; i <= m.groupCount(); i++) {
                count++;
            }
        }
        if (count>0){
            index = true;
        }
        return index;
    }

    public static void main(String[] args) {
        logger.info("您好");
    }
}
