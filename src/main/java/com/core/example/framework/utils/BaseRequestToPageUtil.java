/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.utils;

import com.core.example.framework.request.BaseRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @file: com.chinaunicom.sdsi.framework.utils.BaseRequestToPageUtil
 * @description:
 * @author: Suen
 * @date: 2019-03-01
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
public class BaseRequestToPageUtil {

    private BaseRequestToPageUtil() {
        throw new IllegalStateException("Utility BaseRequestToPageUtil");
    }
    /**
     * 从BaseRequest取分页、排序的值，生成mybatis-plus的ipage对象
     *
     * @param body
     * @return
     */
    @SuppressWarnings("rawtypes")
	public static IPage toPage(BaseRequest body) {
        // 分页数据赋值
        return  new Page(body.getPageNum(), body.getPageSize());
    }
}
