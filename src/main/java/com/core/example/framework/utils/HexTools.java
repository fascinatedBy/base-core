package com.core.example.framework.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * 各种十六进制之间的转换
 * String eeew = "192.168.1.13-0";
 * String bbbb = "895B68513D5EC0684B6D4E0045005700";
 * String ccccc = "3100390032002E003100360038002E0031002E00310033002D003000";
 */
@Slf4j
public class HexTools {
	public static final String CHATSETUTF8 = "UTF-8";////GBK//Unicode//UTF-16LE

	public static void main(String[] args){
		String mmm = "安全帽检测NEW";
		//编码
		String encoder = string2HexUtf8(mmm);
		log.info(encoder);
		//解码
		String decoder = hexUTF82String(encoder);
		log.info(decoder);

	}


	/**
	 * @Title:bytes2HexString
	 * @Description:字节数组转16进制字符串
	 * @param b
	 * 字节数组
	 * @return 16进制字符串
	 * @throws
	 */
	public static String bytes2HexString(byte[] b) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < b.length; i++) {
			result.append(String.format("%02X",b[i]));
		}
		return result.toString();
	}
	/**
	 * 编码.
	 *
	 * @param data 原始数据.
	 * @return 十六进制字符串.
	 */
	public static String encodeToString(byte[] data, boolean isUpperCase) {
		char[] digital = "0123456789abcdef".toCharArray();
		if(isUpperCase){
			digital = "0123456789ABCDEF".toCharArray();
		}
		StringBuilder sb = new StringBuilder("");
		int bit;
		for (int i = 0; i < data.length; i++) {
			bit = (data[i] & 0xF0) >> 4;
			sb.append(digital[bit]);
			bit = data[i] & 0x0F;
			sb.append(digital[bit]);
		}
		return sb.toString();
	}

	/**
	 * @Title:hexString2Bytes
	 * @Description:16进制字符串转字节数组
	 * @param src
	 * 16进制字符串
	 * @return 字节数组
	 * @throws
	 */
	public static byte[] hexString2Bytes(String src) {
		int l = src.length() / 2;
		byte[] ret = new byte[l];
		for (int i = 0; i < l; i++) {
			ret[i] = Integer.valueOf(src.substring(i * 2, i * 2 + 2), 16).byteValue();
		}
		return ret;
	}
	/**
	 * String 类型的十六进制字符串转换为 byte 内存数组
	 *
	 * @param hex
	 *            String 类型的十六进制字符串.
	 * @return byte 内存数组.
	 */
	public static  byte[] decode(String hex) {
		String digital = "0123456789abcdef";
		char[] hex2char = hex.toLowerCase().toCharArray();
		byte[] bytes = new byte[hex.length() / 2];
		int temp;
		for (int i = 0; i < bytes.length; i++) {
			temp = digital.indexOf(hex2char[2 * i]) << 4;
			temp += digital.indexOf(hex2char[2 * i + 1]);
			bytes[i] = (byte) (temp & 0xFF);
		}
		return bytes;
	}
	/**
	 * 十六进制串转化为byte数组
	 *
	 * @return the array of byte
	 */
	public static byte[] hexToByte(String hex){
		int two = 2;
		if (hex.length() % two != 0) {
			throw new IllegalArgumentException("error");
		}
		char[] arr = hex.toCharArray();
		byte[] b = new byte[hex.length() / two];
		for (int i = 0, j = 0, l = hex.length(); i < l; i++, j++) {
			String swap = "" + arr[i++] + arr[i];
			Integer byteInteger = Integer.valueOf(Integer.parseInt(swap, 16) & 0xFF);
			//log.info("{}----ss----->{}",byteInteger.byteValue(),j);
			b[j] = byteInteger.byteValue();
		}
		return b;
	}

	/**
	 * 字节数组转换为十六进制字符串
	 *
	 * @param b byte[] 需要转换的字节数组
	 * @return String 十六进制字符串
	 */
	public static String byteToHex(byte [] b) {
		if (b == null) {
			throw new IllegalArgumentException(
					"Argument b ( byte array ) is null! ");
		}
		StringBuilder hs = new StringBuilder();
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = Integer.toHexString(b[n] & 0xff);
			if (stmp.length() == 1) {
				hs.append("0").append(stmp);
			} else {
				hs.append(stmp);
			}
		}
		return hs.toString().toLowerCase();
	}


	/**
	 * @Title:string2HexUTF8
	 * @Description:字符UTF8串转16进制字符串
	 * @param strPart
	 * 字符串
	 * @return 16进制字符串
	 * @throws
	 */
	public static String string2HexUtf8(String strPart) {

		return string2HexString(strPart,CHATSETUTF8);
	}

	/**
	 * @Title:string2HexString
	 * @Description:字符串转16进制字符串
	 * @param strPart 字符串
	 * @param tochartype hex目标编码
	 * @return 16进制字符串
	 * @throws
	 */
	public static String string2HexString(String strPart, String tochartype) {
		try{
			return bytes2HexString(strPart.getBytes(tochartype));
		}catch (Exception e){
			return "";
		}
	}

	/**
	 * @Title:hexUTF82String
	 * @Description:16进制UTF-8字符串转字符串
	 * @param src
	 * 16进制字符串
	 * @return 字节数组
	 * @throws
	 */
	public static String hexUTF82String(String src) {

		return hexString2String(src,CHATSETUTF8,CHATSETUTF8);
	}

	/**
	 * @Title:hexString2String
	 * @Description:16进制字符串转字符串
	 * @param src
	 * 16进制字符串
	 * @return 字节数组
	 * @throws
	 */
	public static String hexString2String(String src, String oldchartype, String chartype) {
		byte[] bts=hexString2Bytes(src);
		try{
			if(oldchartype.equals(chartype)){
				return new String(bts,oldchartype);
			}
			else{
				return new String(new String(bts,oldchartype).getBytes(),chartype);
			}
		}
		catch (Exception e){

			return"";
		}
	}

	/**
	 * @Title:string2HexUTF8
	 * @Description:字符UTF-16LE串转16进制字符串,此UTF-16LE等同于C#中的Unicode
	 * @param strPart
	 * 字符串
	 * @return 16进制字符串
	 * @throws
	 */
	public String string2HexUTF16LE(String strPart) {

		return string2HexString(strPart,"UTF-16LE");
	}

	///////////////////////////////////////////////////
	/////////////////////////////////////////////////

	/**
	 * @Title:string2HexUnicode
	 * @Description:字符Unicode串转16进制字符串
	 * @param strPart
	 * 字符串
	 * @return 16进制字符串
	 * @throws
	 */
	public String string2HexUnicode(String strPart) {

		return string2HexString(strPart,"Unicode");
	}

	/**
	 * @Title:string2HexGBK
	 * @Description:字符GBK串转16进制字符串
	 * @param strPart
	 * 字符串
	 * @return 16进制字符串
	 * @throws
	 */
	public String string2HexGBK(String strPart) {

		return string2HexString(strPart,"GBK");
	}

	/**
	 * @Title:hexUTF16LE2String
	 * @Description:16进制UTF-8字符串转字符串，,此UTF-16LE等同于C#中的Unicode
	 * @param src
	 * 16进制字符串
	 * @return 字节数组
	 * @throws
	 */
	public String hexUTF16LE2String(String src) {

		return hexString2String(src,"UTF-16LE",CHATSETUTF8);
	}

	/**
	 * @Title:hexGBK2String
	 * @Description:16进制GBK字符串转字符串
	 * @param src
	 * 16进制字符串
	 * @return 字节数组
	 * @throws
	 */
	public String hexGBK2String(String src) {

		return hexString2String(src,"GBK",CHATSETUTF8);
	}

	/**
	 * @Title:hexUnicode2String
	 * @Description:16进制Unicode字符串转字符串
	 * @param src
	 * 16进制字符串
	 * @return 字节数组
	 * @throws
	 */
	public String hexUnicode2String(String src) {
		return hexString2String(src,"Unicode",CHATSETUTF8);
	}
}