package com.core.example.framework.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 高效分布式ID生成算法(sequence),基于Snowflake算法优化实现64位自增ID算法。
 * 其中解决时间回拨问题的优化方案如下：
 * 1. 如果发现当前时间少于上次生成id的时间(时间回拨)，着计算回拨的时间差
 * 2. 如果时间差(offset)小于等于5ms，着等待 offset * 2 的时间再生成
 * 3. 如果offset大于5，则直接抛出异常
 */
public class IdGenerator {
    private static Sequence WORKER = new Sequence();

    public static long getId() {
        return WORKER.nextId();
    }

    public static String getIdStr() {
        return String.valueOf(WORKER.nextId());
    }

    public static void main(String[] args) {
        final List<Long> ids=new ArrayList<>();
        new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    final int j=i;
                    new Thread(new Runnable() {
                        public void run() {
                            ids.add(getId());
                        }
                    }).start();
                }
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    final int j=i;
                    new Thread(new Runnable() {
                        public void run() {
                            ids.add(getId());
                        }
                    }).start();
                }
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    final int j=i;
                    new Thread(new Runnable() {
                        public void run() {
                            ids.add(getId());
                        }
                    }).start();
                }
            }
        }).start();

        HashSet<Long> hashSet = new HashSet<Long>(ids);
        if (ids.size() != hashSet.size()) {
            System.out.println("list中存在重复的数据");
        }
        System.out.println("无重复数据");
    }
}
