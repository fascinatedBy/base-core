package com.core.example.framework.utils;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Base64;
import java.util.zip.GZIPOutputStream;

public class StringCompression {
    public static void main(String[] args) throws IOException {
        // 要压缩的原始字符串
        String originalStr = "eyJhbGciOiJBRVMiLCJ0eXAiOiJKV1QifQ==.eyJleHAiOiIxMDgwMDAwMCIsImlhdCI6IjE3MDQ4NTc1MDgwMDciLCJpc3MiOiJ3d3cubGl1Z3VpcWluZy5jb20iLCJ1c2VyRGF0YSI6e319.K1p4NnNiM2lKWmFNOEh1cFkzcmdTc3VvN1pIbEZtdEFNSVNSOUMzYmtGMkIwVGVWWGNvNVEyQUVjY2E3eG9ON3NPWXpwdnJOR2lvMA0KcC9HNkpia0g5NG1kcnpORXFCYTJyeGFudC9qRnlldFdnMXhOcmRDR1FjNzM1aU45T2xxNWdVZ3VWdEcwK2FiajhjZjlxektmTmc9PQ==";
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPOutputStream gzos = new GZIPOutputStream(baos);
        OutputStreamWriter osw = new OutputStreamWriter(gzos);
        BufferedWriter bw = new BufferedWriter(osw);
        
        try {
            bw.write(originalStr);
            
            byte[] compressedBytes = baos.toByteArray();
            System.out.println("压缩后的字节数组长度：" + compressedBytes.length);
            
            // 将字节数组转换为Base64编码的字符串
            String compressedStr = Base64.getEncoder().encodeToString(compressedBytes);
            System.out.println("压缩后的字符串：" + compressedStr);
        } finally {
            bw.close();
            osw.close();
            gzos.close();
            baos.close();
        }
    }
}