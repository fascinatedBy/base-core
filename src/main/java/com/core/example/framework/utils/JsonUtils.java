package com.core.example.framework.utils;/**
 * Created by chentao on 2021/1/28.
 */

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: chentao
 * @time: 2021/1/28 19:18
 */
public class JsonUtils {
    public static List jsonArrayToList(String json)
    {
        List list = new ArrayList();
        JSONArray jsonArray = JSONArray.fromObject(json);
        for(int i = 0; i < jsonArray.size(); i++)
        {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            list.add(jsonToMap(jsonObject));
        }

        return list;
    }

    public static Map jsonToMap(JSONObject jsonObject)
    {
        Map result = new HashMap();
        jsonToMap(jsonObject, result);
        return result;
    }

    public static void jsonToMap(JSONObject o, Map result)
            throws JSONException
    {
        Iterator it1 = o.keys();
        Object jsValue = null;
        while(it1.hasNext())
        {
            String jsonKey = (String)it1.next();
            jsValue = o.get(jsonKey);
            if(jsValue instanceof JSONObject)
            {
                Map sub = new HashMap();
                result.put(jsonKey, sub);
                jsonToMap((JSONObject)jsValue, sub);
            } else
            {
                if(jsValue==null||jsValue.toString().equals("null")) {
                    jsValue="";
                }
                result.put(jsonKey, jsValue);
            }
        }
    }

}
