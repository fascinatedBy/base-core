package com.core.example.framework.utils;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: chentao
 * @time: 2024/1/16 10:03
 */
@Component
public class TokenScheme {

    /**
     * 获取申请次数次数
     *
     * @param appKey
     * @return
     */
    @Cacheable(value = "authCount#appKey.appCount", key = "#appKey")
    public Integer getAppKeyCount(String appKey) {
        return 0;
    }

    /**
     * 更新申请次数
     *
     * @param appKey
     * @param appKeyCount
     */
    @CachePut(value = "authCount#appKey.appCount", key = "#appKey")
    public Integer setAppKeyCount(String appKey, Integer appKeyCount) {
        return appKeyCount;
    }

}
