package com.core.example.framework.utils;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

public class EncryptionUtil {
    private static final String ALGORITHM = "RSA";
 
    public static String encrypt(String input, PublicKey publicKey) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] encryptedBytes = cipher.doFinal(input.getBytes());
            return Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
 
    public static String decrypt(String input, PrivateKey privateKey) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] decodedBytes = Base64.getDecoder().decode(input);
            byte[] decryptedBytes = cipher.doFinal(decodedBytes);
            return new String(decryptedBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
 
    public static void main(String[] args) {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();
 
            String input = "eyJhbGciOiJBRVMiLCJ0eXAiOiJKV1QifQ==.eyJleHAiOiIxMDgwMDAwMCIsImlhdCI6IjE3MDQ4NTc1MDgwMDciLCJpc3MiOiJ3d3cubGl1Z3VpcWluZy5jb20iLCJ1c2VyRGF0YSI6e319.K1p4NnNiM2lKWmFNOEh1cFkzcmdTc3VvN1pIbEZtdEFNSVNSOUMzYmtGMkIwVGVWWGNvNVEyQUVjY2E3eG9ON3NPWXpwdnJOR2lvMA0KcC9HNkpia0g5NG1kcnpORXFCYTJyeGFudC9qRnlldFdnMXhOcmRDR1FjNzM1aU45T2xxNWdVZ3VWdEcwK2FiajhjZjlxektmTmc9PQ==";
            String encrypted = encrypt(input, publicKey);
            System.out.println("Encrypted: " + encrypted);
            String decrypted = decrypt(encrypted, privateKey);
            System.out.println("Decrypted: " + decrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}