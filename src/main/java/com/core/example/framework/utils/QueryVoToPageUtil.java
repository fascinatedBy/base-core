package com.core.example.framework.utils;

import com.core.example.framework.entity.BaseQueryVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

/**
 * 将BaseQueryVO的参数转变成分页参数的工具类
 *
 * @file: com.chinaunicom.sdsi.framework.utils.QueryVoToPageUtil
 * @description:
 * @author: wangwj
 * @date: 2019-01-09
 * @version: V1.0
 * @copyright: Copyright(c) 2019 Sdcncsi Co. Ltd. All rights reserved.
 */
public class QueryVoToPageUtil {

    private QueryVoToPageUtil(){
        //this is empty
    }
    /**
     * 从baseQueryVO取分页、排序的值，生成mybatis-plus的ipage对象
     *
     * @param queryVO
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static IPage toPage(BaseQueryVO queryVO) {
        // 分页数据赋值
        Page page = new Page(queryVO.getPageNum(), queryVO.getPageSize());
        // 排序数据赋值
        if (queryVO.isOrdered()) {
            List<String> ascs = Lists.newArrayList();
            List<String> descs = Lists.newArrayList();
            Arrays.stream(queryVO.getOrder()).forEach(t -> {
                if (UnifastConstants.ORDER_ASC.equals(t.get(UnifastConstants.ORDER_DIR))) {
                    ascs.add(t.get(UnifastConstants.ORDER_COLUMN));
                } else {
                    descs.add(t.get(UnifastConstants.ORDER_COLUMN));
                }
            });
            page.setAscs(ascs);
        }
        return page;
    }
}
