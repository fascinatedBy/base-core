package com.core.example.framework.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.zip.GZIPInputStream;

public class StringDecompression {
    public static void main(String[] args) throws IOException {
        // 要解压的字符串（需先经过压缩）
        String compressedStr = "H4sIAAAAAAAAAA==";
        
        byte[] compressedBytes = Base64.getDecoder().decode(compressedStr);
        ByteArrayInputStream bais = new ByteArrayInputStream(compressedBytes);
        GZIPInputStream gzis = new GZIPInputStream(bais);
        InputStreamReader isr = new InputStreamReader(gzis);
        BufferedReader br = new BufferedReader(isr);
        
        StringBuilder decompressedBuilder = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            decompressedBuilder.append(line);
        }
        
        String decompressedStr = decompressedBuilder.toString();
        System.out.println("解压后的字符串：" + decompressedStr);
        
        br.close();
        isr.close();
        gzis.close();
        bais.close();
    }
}