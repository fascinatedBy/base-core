/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.utils;

/**
 * 框架全局常量类
 *
 * @file: com.chinaunicom.sdsi.framework.utils.UnifastConstants
 * @description:
 * @author: wangwj
 * @date: 2019-01-14
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
public class UnifastConstants {

    private UnifastConstants() {
        // this is empty
    }

    /**
     * 缓存中当前用户的Key
     */
    public static final String CURRENT_USER_CACHE_KEY = "MALL_CURRENT_USER";

    public static final Integer INTEGER_ONE = 1;

    public static final Integer INTEGER_ZERO = 0;

    public static final Integer INTEGER_MINUS_ONE = -1;

    public static final String ERROR_NOT_FOUND = "未查询到数据";

    public static final String ERROR_BAD_REQUEST = "请求参数不符合要求";

    public static final String ORDER_ASC = "asc";

    public static final String ORDER_DESC = "desc";

    public static final String ORDER_DIR = "dir";

    public static final String ORDER_COLUMN = "column";


}
