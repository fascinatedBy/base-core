package com.core.example.framework.utils;

/**
 * 生成的随机数类型
 *
 * @author unifast
 */
public enum RandomType {
	/**
	 * INT STRING ALL
	 */
	INT, STRING, ALL
}
