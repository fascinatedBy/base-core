package com.core.example.framework.utils;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils {
    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 存入缓存
     *
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key, Object value) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, value);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @Description: 删除缓存
     * @Author: hj
     * @Date: 16:51 2017/10/24
     */
    public void remove(final String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    /**
     * @Description: 写入缓存(可以配置过期时间)
     * @Author: hj
     * @Date: 16:46 2017/10/24
     */
    public boolean set(final String key, Object value, Long expireTime) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, value);
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @Description: 读取缓存
     * @Author: hj
     * @Date: 16:49 2017/10/24
     */
    public Object get(final String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * @Description: 判断缓存中是否有对应的value
     * @Author: lijunjie
     */
    public boolean exists(final String key) {
        return redisTemplate.hasKey(key);
    }


    /**********************操作 map *********/

    /**
     * 存储或更新hash
     *
     * @param redisId
     * @param map
     * @return
     */
    public String setMap(String redisId, Map<String, String> map) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        hashOperations.putAll(redisId, map);
        return redisId;
    }

    /**
     * 获取map
     *
     * @param redisId
     * @return
     */
    public Map<String, String> getMap(String redisId) {
        Map<String, String> result = null;
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        result = hashOperations.entries(redisId);
        return result;
    }

    /**
     * 获取map 指定key 对应value
     *
     * @param redisId
     * @param keyId
     * @return
     */
    public String getMapValue(String redisId, String keyId) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        return hashOperations.get(redisId, keyId);
    }

    public void deletedMapByKeys(String redisId, String... keys) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        hashOperations.delete(redisId, keys);
    }

    /**
     * 批量删除
     * @param keyIds
     */
    /*public void deletedMapByKeys(List<String> keyIds){
        redisTemplate.setKeySerializer(new JdkSerializationRedisSerializer());
        redisTemplate.delete(keyIds);
    }*/

    /**********************操作 map *********/


    /*****操作 set****/
    /**
     * 新增set
     *
     * @param redisId
     * @param sets
     * @return
     */
    public String saveSet(String redisId, Set<String> sets) {
        SetOperations<String, String> setOperations = redisTemplate.opsForSet();
        Long result = setOperations.add(redisId, sets.toString());
        return result + "";
    }

    /**
     * 新增key
     *
     * @param redisId
     * @param value
     * @return
     */
    public String addSet(String redisId, String value) {
        SetOperations<String, String> setOperations = redisTemplate.opsForSet();
        Long result = setOperations.add(redisId, value);
        return result + "";
    }

    public String removeSet(String redisId, String value) {
        SetOperations<String, String> setOperations = redisTemplate.opsForSet();
        Long result = setOperations.remove(redisId, value);
        return result + "";
    }

    public Set<String> getSet(String redisId) {
        SetOperations<String, String> setOperations = redisTemplate.opsForSet();
        Set<String> result = setOperations.members(redisId);
        return result;
    }

    /*****操作 set****/

    /**
     * 设置过期时间
     *
     * @return
     */
    public Boolean expire(final String key, Long timeOut) {
        return redisTemplate.expire(key, timeOut, TimeUnit.SECONDS);
    }

    public Long incrementHash(String key, String hashKey, Long delta) {
        try {
            if (null == delta) {
                delta = 1L;
            }
            return redisTemplate.opsForHash().increment(key, hashKey, delta);
        } catch (Exception e) {//redis宕机时采用uuid的方式生成唯一id
            int first = new Random(10).nextInt(8) + 1;
            int randNo = UUID.randomUUID().toString().hashCode();
            if (randNo < 0) {
                randNo = -randNo;
            }
            return Long.valueOf(first + String.format("%16d", randNo));
        }
    }

    /**
     * Redis模糊匹配key 批量删除
     *
     * @param indistinctKey 模糊匹配的key 如*prex*
     */
    public void deleteByIndistinctKey(String indistinctKey) {
        Set<String> keys = redisTemplate.keys(indistinctKey);
        if (CollectionUtils.isNotEmpty(keys)) {
            redisTemplate.delete(keys);
        }
    }
}
