package com.core.example.framework.utils;

import org.apache.tomcat.util.codec.binary.Base64;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


/**
 * @ClassName
 * @Description 加密工具类
 * @Author ct
 * @Date 14:54 2022/11/14
 * @Version V1.0
 **/
public class SM2Utils {
    private static Logger logger = LoggerFactory.getLogger(SM2Utils.class);
 
    private static final SM2Engine.Mode DIGEST = SM2Engine.Mode.C1C3C2;
 
 
    /**
     * @Description: 生成sm2秘钥对
     * @Author: csn
     * @date: 2022/11/14
     */
    public static KeyPair createSm2Key() {
        try {
            //使用标准名称创建EC参数生成的参数规范
            final ECGenParameterSpec sm2p256v1 = new ECGenParameterSpec("sm2p256v1");
            // 获取一个椭圆曲线类型的密钥对生成器
            final KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", new BouncyCastleProvider());
            // 使用SM2的算法区域初始化密钥生成器
            kpg.initialize(sm2p256v1, new SecureRandom());
            // 获取密钥对
            return kpg.generateKeyPair();
        } catch (Exception e) {
            logger.error("生成秘钥对失败{}", e.getMessage());
            return null;
        }
    }

    /**
     * sm2公钥加密方法
     * @param publicKeyStr 加密的公钥
     * @param data         需要加密的数据
     * @return 返回加密后的字符串
     */
    public static String encryptSm2(String publicKeyStr, String data) {
        try {
            //算法工具包
            Security.addProvider(new BouncyCastleProvider());
            //将公钥字符串转为公钥字节
            byte[] bytes = Base64.decodeBase64(publicKeyStr);
            KeyFactory keyFactory = KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            logger.info("获取转换后的公钥");
            PublicKey publicKey = keyFactory.generatePublic(keySpec);
 
            logger.info("开始加密");
            CipherParameters pubKeyParameters = new ParametersWithRandom(ECUtil.generatePublicKeyParameter(publicKey), new SecureRandom());
            SM2Engine sm2Engine = new SM2Engine();
            sm2Engine.init(true, pubKeyParameters);
            byte[] arrayBytes = sm2Engine.processBlock(data.getBytes(), 0, data.getBytes().length);
            return Base64.encodeBase64String(arrayBytes);
            //开始加密
        } catch (Exception e) {
            logger.error("加密失败{}", e.getMessage());
        }
        return null;
 
    }
 
    /**
     * sm2私钥解密方法
     * @param privateStr 私钥
     * @param data       需要解密的数据
     * @return 返回解密后的数据
     */
    public static String decryptSm2(String privateStr, String data) {
        try {
            logger.info("私钥转换");
            byte[] bytes = Base64.decodeBase64(privateStr);
            KeyFactory keyFactory = KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
            PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
            logger.info("sm2开始解密");
 
            CipherParameters privateKeyParameters = ECUtil.generatePrivateKeyParameter((BCECPrivateKey) privateKey);
            SM2Engine engine = new SM2Engine();
            engine.init(false, privateKeyParameters);
            byte[] byteDate = engine.processBlock(Base64.decodeBase64(data), 0, Base64.decodeBase64(data).length);
            return new String(byteDate);
        } catch (Exception e) {
            logger.error("sm2解密失败{}", e.getMessage());
            return null;
        }
    }
 
    public static void main(String[] args) throws Exception {
        //定义需要加密的字符串
        String str = "aaaaa";
        //生成秘钥对
        KeyPair sm2Key = createSm2Key();
        //获取公钥
        PublicKey publicKey = sm2Key.getPublic();
        //获取公钥base加密后字符串
        String publicStr = Base64.encodeBase64String(publicKey.getEncoded());
        logger.info("公钥为：{}", publicStr);
        //获取私钥
        PrivateKey privateKey = sm2Key.getPrivate();
        //获取私钥base加密后字符串
        String privateStr = Base64.encodeBase64String(privateKey.getEncoded());
        logger.info("私钥为：{}", privateStr);

        //公钥加密
        String passStr = encryptSm2(publicStr, str);
        logger.info("加密后为{}", passStr);
        //私钥解密
        String deStr = decryptSm2(privateStr, passStr);
        logger.info("解密后为{}", deStr);
    }
}