package com.core.example.framework.utils;

import com.core.example.cloud.auth.MallUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * unifast上下文拓展信息
 *
 * @author zfc
 */
@Component
public class UnifastContext {

    /**
     * 获取默认用户对象
     *
     * @return MallUser
     */
    public MallUser getUser() {
            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated()) {
            if (authentication.getPrincipal() instanceof MallUser) {
                return (MallUser) authentication.getPrincipal();
            }
        }
        return null;
    }

    /**
     * 获取自定义拓展对象
     *
     * @return
     */
    public Object getCustomerParamsByKey(String key) {
        Map customerParam = getUser().getCustomParam();
        return customerParam.get(key);
    }
}
