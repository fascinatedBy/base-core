package com.core.example.framework.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * 流水号生成工具类
 * 借助redis的incr，每自然日重新计数
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/2/22
 */
@Component
public final class SerialNumUtil {
    /**
     * Redis缓存前缀
     */
    private static final String SEQUENCE_KEY = "UNI_CACHE_SEQUENCE";
    /**
     * 返回值位数，默认六位，如000001
     */
    @Value("${unifast.serial-number-length:6}")
    private Integer number;
    /**
     * RedisTemplate
     */
    @Resource
    private RedisTemplate<String, Long> redisTemplate;

    /**
     * 根据业务标识获取流水号
     *
     * @param business 业务标识符
     * @return 流水号
     */
    public String fetchSequence(String business) {
        // 缓存键
        String key = SEQUENCE_KEY + "::" + business + "::" + LocalDate.now().toString();
        // 判断是否存在
        final Boolean hasKey = redisTemplate.hasKey(key);
        // 取值
        Long increment = redisTemplate.opsForValue().increment(key, 1L);
        if (Boolean.FALSE.equals(hasKey)) {
            //如果key是新创建的，那么设置过期时间，时长为当前时间到凌晨12点的秒数
            LocalDateTime now = LocalDateTime.now();
            final LocalDateTime midNight = now.plusDays(1L).withHour(0).withMinute(0).withSecond(0).withNano(0);
            final long between = ChronoUnit.SECONDS.between(now, midNight);
            redisTemplate.expire(key, Duration.of(between, ChronoUnit.SECONDS));
        }
        // 左补零
        return this.leftPad(increment + "", number, "0");
    }

    /**
     * 左补齐
     * 参考org.apache.commons.lang3.StringUtils.leftPad(final String str, final int size, String padStr)
     * 为了减少依赖，拷贝出来进行简化
     *
     * @param str    String
     * @param size   int
     * @param padStr String
     * @return String
     */
    private String leftPad(final String str, final int size, String padStr) {
        if (str == null) {
            return null;
        }
        final int padLen = padStr.length();
        final int strLen = str.length();
        final int pads = size - strLen;
        if (pads <= 0) {
            return str;
        }
        if (pads == padLen) {
            return padStr.concat(str);
        } else if (pads < padLen) {
            return padStr.substring(0, pads).concat(str);
        } else {
            final char[] padding = new char[pads];
            final char[] padChars = padStr.toCharArray();
            for (int i = 0; i < pads; i++) {
                padding[i] = padChars[i % padLen];
            }
            return new String(padding).concat(str);
        }
    }

}
