package com.core.example.framework.utils;

import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description:
 * @author: chentao
 * @time: 2024/1/16 10:48
 */
@Component
public class SchemeUtil {

    @Setter
    private TokenScheme tokenScheme;

        public boolean isRelease(String key){
//        if (!isScheme){
//            return true;
//        }
        String appKey = key + "AuthConstant.AK_SCHEME";

        AtomicInteger appKeyCount = new AtomicInteger(tokenScheme.getAppKeyCount(appKey));
        if (appKeyCount == null) {
            appKeyCount = new AtomicInteger();
            tokenScheme.setAppKeyCount(appKey, appKeyCount.get());
            return true;
        }
        if (appKeyCount.incrementAndGet() > 10000) {
            return false;
        }
        tokenScheme.setAppKeyCount(appKey, appKeyCount.get()+1);
        return true;
    }

}
