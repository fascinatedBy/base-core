package com.core.example.framework.utils;

import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * @description:
 * @author: chentao
 * @time: 2024/1/10 14:22
 */
public class CompressionExample {

    public static void main(String[] args) throws DataFormatException {
        String originalstring = "Hello, world!";
        // 压缩
        byte[] compressedBytes = new byte[1024];
        Deflater deflater = new Deflater();
        deflater.setInput(originalstring.getBytes());
        deflater.finish();
        int compressedLength = deflater.deflate(compressedBytes);
        System.out.println("compressedLength = " + compressedLength);
        // 解压缩
        Inflater inflater = new Inflater();
        inflater.setInput(compressedBytes,0,compressedLength);
        byte[] decompressedBytes = new byte[1024];
        int decompressedLength = inflater.inflate(decompressedBytes);
        inflater.end();
        String decompressedstring = new String(decompressedBytes, 0, decompressedLength);
        System.out.println("decompressedstring = " + decompressedstring);
    }
}
