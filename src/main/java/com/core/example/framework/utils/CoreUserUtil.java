package com.core.example.framework.utils;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Optional;

public class CoreUserUtil {
    private CoreUserUtil() {
    }

    public static String getName() {
        Optional<RequestAttributes> attributes = Optional.ofNullable(RequestContextHolder.getRequestAttributes());
        if (attributes.isPresent()) {
            HttpServletRequest request = ((ServletRequestAttributes) attributes.get()).getRequest();
            Optional<Principal> user = Optional.ofNullable(request.getUserPrincipal());
            if (user.isPresent()) {
                return (user.get()).getName();
            }
        }
        return null;
    }
}
