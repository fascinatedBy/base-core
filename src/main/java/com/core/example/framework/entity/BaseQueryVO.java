package com.core.example.framework.entity;

import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@ApiModel("查询请求基类")
public class BaseQueryVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("绘画计数器")
    private Integer draw;
    @ApiModelProperty(
            value = "当前页",
            hidden = true
    )
    private Integer pageNum = 1;
    @ApiModelProperty(
            value = "每页的数量",
            hidden = true
    )
    private Integer pageSize = 10;
    @ApiModelProperty("起始记录行数")
    private Integer start;
    @ApiModelProperty("每页的数量")
    private Integer length;
    @ApiModelProperty(
            value = "排序字段的前缀",
            hidden = true
    )
    private String orderColumnPrefix;
    @ApiModelProperty(
            value = "排序",
            hidden = true
    )
    @Getter
    @Setter
    private HashMap<String, String>[] order;
    private String[] orderColumns;
    private String[] orderDirs;

    public Integer getPageNum() {
        if (null != this.start && null != this.length) {
            this.pageNum = this.start / this.length + 1;
        }

        return this.pageNum;
    }

    public Integer getPageSize() {
        if (null != this.start && null != this.length) {
            this.pageSize = this.length;
        }

        return this.pageSize;
    }

    public boolean isOrdered() {
        return this.getOrderColumns() != null && this.getOrderColumns().length > 0;
    }

    public boolean isOrdered(String column) {
        Assert.hasText(column, "请不要传空值");
        return Arrays.binarySearch(this.getOrderColumns(), column) >= 0;
    }

    public String[] getOrderColumns() {
        if (this.order != null && this.order.length > 0) {
            if (this.orderColumns != null && this.orderColumns.length > 0) {
                return this.orderColumns;
            } else {
                this.orderColumns = new String[this.order.length];
                int i = 0;

                for (int j = this.order.length; i < j; ++i) {
                    this.orderColumns[i] = this.order[i].get("column");
                }

                return this.orderColumns;
            }
        } else {
            return new String[0];
        }
    }

    public String[] getOrderDirs() {
        if (this.order != null && this.order.length > 0) {
            if (this.orderDirs != null && this.orderDirs.length > 0) {
                return this.orderDirs;
            } else {
                this.orderDirs = new String[this.order.length];
                int i = 0;

                for (int j = this.order.length; i < j; ++i) {
                    this.orderDirs[i] = this.order[i].get("dir");
                }

                return this.orderDirs;
            }
        } else {
            return new String[0];
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public String getOrderString() {
        if (this.order != null && this.order.length > 0) {
            StringBuilder sb = new StringBuilder(30);
            Iterator var2 = Lists.newArrayList(this.order).iterator();

            while (var2.hasNext()) {
                Map<String, String> m = (Map) var2.next();
                String column = m.get("column");
                if (StringUtils.hasLength(column)) {
                    column = com.core.example.framework.utils.StringUtils.underscoreName(column);
                    sb.append("CONVERT(");
                    if (this.orderColumnPrefix != null && this.orderColumnPrefix.length() > 0) {
                        sb.append(this.orderColumnPrefix).append(".");
                    }
                    sb.append(column).append(" USING gbk) ").append((String) m.get("dir")).append(",");
                }
            }

            if (sb.length() > 1) {
                return sb.substring(0, sb.length() - 1);
            }
            return "";
        }

        return "";
    }

    public BaseQueryVO() {
        // this is empty
    }

    public Integer getDraw() {
        return this.draw;
    }

    public Integer getStart() {
        return this.start;
    }

    public Integer getLength() {
        return this.length;
    }

    public String getOrderColumnPrefix() {
        return this.orderColumnPrefix;
    }

    public void setDraw(final Integer draw) {
        this.draw = draw;
    }

    public void setPageNum(final Integer pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
        this.length = pageSize;
    }

    public void setOrderColumns(final String[] orderColumns) {
        this.orderColumns = orderColumns;
    }

    public void setOrderDirs(final String[] orderDirs) {
        this.orderDirs = orderDirs;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof BaseQueryVO)) {
            return false;
        }
        BaseQueryVO other = (BaseQueryVO) o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this0draw = this.getDraw();
        Object other0draw = other.getDraw();

        if (check(this0draw, other0draw)) {
            return false;
        }

        Object this0pageNum = this.getPageNum();
        Object other0pageNum = other.getPageNum();

        if (check(this0pageNum, other0pageNum)) {
            return false;
        }

        Object this0pageSize = this.getPageSize();
        Object other0pageSize = other.getPageSize();

        if (check(this0pageSize, other0pageSize)) {
            return false;
        }

        Object this0start = this.getStart();
        Object other0start = other.getStart();

        if (check(this0start, other0start)) {
            return false;
        }

        Object this0length = this.getLength();
        Object other0length = other.getLength();
        if (check(this0length, other0length)) {
            return false;
        }

        Object this0orderColumnPrefix = this.getOrderColumnPrefix();
        Object other0orderColumnPrefix = other.getOrderColumnPrefix();

        if (check(this0orderColumnPrefix, other0orderColumnPrefix)) {
            return false;
        }
        return !(!Arrays.deepEquals(this.getOrder(), other.getOrder()) ||
                !Arrays.deepEquals(this.getOrderColumns(), other.getOrderColumns()) ||
                !Arrays.deepEquals(this.getOrderDirs(), other.getOrderDirs()));
    }

    private boolean check(Object thisObj, Object otherObj) {
        return thisObj != null && !thisObj.equals(otherObj);
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BaseQueryVO;
    }

    @Override
    public int hashCode() {
        int result = 1;
        Object sdraw = this.getDraw();
        result = result * 59 + (sdraw == null ? 43 : sdraw.hashCode());
        Object spageNum = this.getPageNum();
        result = result * 59 + (spageNum == null ? 43 : spageNum.hashCode());
        Object spageSize = this.getPageSize();
        result = result * 59 + (spageSize == null ? 43 : spageSize.hashCode());
        Object sstart = this.getStart();
        result = result * 59 + (sstart == null ? 43 : sstart.hashCode());
        Object slength = this.getLength();
        result = result * 59 + (slength == null ? 43 : slength.hashCode());
        Object sorderColumnPrefix = this.getOrderColumnPrefix();
        result = result * 59 + (sorderColumnPrefix == null ? 43 : sorderColumnPrefix.hashCode());
        result = result * 59 + Arrays.deepHashCode(this.getOrder());
        result = result * 59 + Arrays.deepHashCode(this.getOrderColumns());
        result = result * 59 + Arrays.deepHashCode(this.getOrderDirs());
        return result;
    }

    @Override
    public String toString() {
        return "BaseQueryVO(draw=" + this.getDraw() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ", start=" + this.getStart() + ", length=" + this.getLength() + ", orderColumnPrefix=" + this.getOrderColumnPrefix() + ", order=" + Arrays.deepToString(this.getOrder()) + ", orderColumns=" + Arrays.deepToString(this.getOrderColumns()) + ", orderDirs=" + Arrays.deepToString(this.getOrderDirs()) + ")";
    }

    public void setStart(final Integer start) {
        this.start = start;
    }

    public void setLength(final Integer length) {
        this.length = length;
        this.pageSize = length;
    }

    public void setOrderColumnPrefix(final String orderColumnPrefix) {
        this.orderColumnPrefix = orderColumnPrefix;
    }

}
