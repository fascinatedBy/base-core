package com.core.example.framework.response;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.experimental.Accessors;

/**
 * 分页返回对象
 *
 * @param <T>
 * @author wangwj
 */
@Accessors(chain = true)
@ApiModel("分页返回对象")
public class BasePageResponse<T> extends BaseResponse<BasePageVO<T>> {
    @ApiModelProperty(hidden = true)
    private static final long serialVersionUID = 1L;

    public BasePageResponse(IPage<T> page, Integer draw, String error) {
        super(new BasePageVO<>(page, draw, error));
    }

    public BasePageResponse(IPage<T> page, Integer draw) {
        super(new BasePageVO<>(page, draw));
    }

    public BasePageResponse(IPage<T> page) {
        super(new BasePageVO<>(page));
    }

}
