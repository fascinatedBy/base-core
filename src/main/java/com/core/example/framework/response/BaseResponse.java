/*
 * Copyright (c) 2019, SDCNCSI. All rights reserved.
 */

package com.core.example.framework.response;

import com.core.example.framework.enums.ResponseEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统返回值封装类
 * 所有返回值均用此类型，status为系统状态码，message为响应消息，data为响应数据
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 * update   1    2020-11-16  wangwj  增加code、success，status改为transient
 *
 * @author wangwj
 * @version V1.0
 * @date 2019-02-25
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
public class BaseResponse<T> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * 业务状态码
     */
    private String code;
    /**
     * 响应状态
     */
    private boolean success;
    /**
     * 状态码
     */
    transient String status = ResponseEnum.SUCCESS.getCode();
    /**
     * 消息
     */
    String message = ResponseEnum.SUCCESS.getMsg();
    /**
     * 数据
     */
    T data;

    /**
     * 无参构造
     */
    public BaseResponse() {
    }

    /**
     * 只传数据的构造方法，默认状态码为success（1）
     *
     * @param data T
     */
    public BaseResponse(T data) {
        this(ResponseEnum.SUCCESS.getCode(), ResponseEnum.SUCCESS.getMsg(), data);
    }

    /**
     * 接收状态码、消息的构造方法，数据默认为空
     *
     * @param status  String
     * @param message String
     */
    public BaseResponse(String status, String message) {
        this(status, message, null);
    }

    /**
     * 接收状态码、消息的构造方法，数据
     *
     * @param status  String
     * @param message String
     * @param data    T
     */
    public BaseResponse(String status, String message, T data) {
        this.setStatus(status);
        this.message = message;
        this.data = data;
        this.setSuccess(ResponseEnum.SUCCESS.getCode().equals(status));
    }

    /**
     * 自定义返回结构
     * @param code
     * @param success
     * @param message
     * @param data
     */
    public BaseResponse(String code,Boolean success,String message, T data) {
        this.message = message;
        this.data = data;
        this.setCode(code);
        this.setSuccess(success);
    }
    /**
     * 接收枚举对象的方法
     *
     * @param respEnum ResponseEnum
     */
    public void setResponseStatus(ResponseEnum respEnum) {
        this.setStatus(respEnum.getCode());
        this.setSuccess(ResponseEnum.SUCCESS.getCode().equals(status));
        this.message = respEnum.getMsg();
    }

    public void setStatus(String status) {
        this.status = status;
        this.code = status;
    }
}
