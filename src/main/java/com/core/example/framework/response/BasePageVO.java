package com.core.example.framework.response;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页返回值数据对象
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2020/11/17
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
@Data
@ApiModel("分页返回值数据对象")
public class BasePageVO<T> implements Serializable {


    @ApiModelProperty(hidden = true)
    private static final long serialVersionUID = 1L;
    /**
     * 总页数
     */
    @ApiModelProperty("总页数")
    private Long total;
    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Long page;
    /**
     * 绘画计数器，可恒为0
     */
    @ApiModelProperty("绘画计数器")
    private Integer draw;
    /**
     * 异常信息
     */
    @ApiModelProperty("异常信息")
    private String error;
    /**
     * 数据列表
     */
    @ApiModelProperty("数据列表")
    private List<T> records;

    /**
     * 基于IPage的构造方法
     *
     * @param page IPage
     */
    public BasePageVO(IPage<T> page) {
        this(page, 0);
    }

    /**
     * 基于IPage、draw的构造方法
     *
     * @param page IPage
     * @param draw Integer
     */
    public BasePageVO(IPage<T> page, Integer draw) {
        this(page, draw, null);
    }


    /**
     * 基于IPage、draw、error的构造方法
     *
     * @param page  IPage
     * @param draw  Integer
     * @param error String
     */
    public BasePageVO(IPage<T> page, Integer draw, String error) {
        this.total = page.getTotal();
        this.page = page.getCurrent();
        this.draw = draw;
        this.records = page.getRecords();
        this.error = error;
    }
}
