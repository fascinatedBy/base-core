package com.core.example.framework.log.dao;


import com.core.example.framework.log.entity.SysLogPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

/**
 * <p>
 * 系统操作日志表 Mapper 接口
 * </p>
 *
 * @author wangwj
 * @since 2019-01-15
 */
@Mapper
@ConditionalOnProperty(value = "unifast.operate-logger-type", havingValue = "jdbc")
public interface SysLogMapper extends BaseMapper<SysLogPO> {


}
