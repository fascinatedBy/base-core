package com.core.example.framework.log.service.impl;

import cn.chinaunicom.sdsi.core.interfaces.OperateLogger;
import cn.chinaunicom.sdsi.core.interfaces.Operation;
import com.core.example.framework.log.dao.SysLogMapper;
import com.core.example.framework.log.entity.SysLogPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/1/27
 */
@Slf4j
@Component
@ConditionalOnProperty(value = "unifast.operate-logger-type", havingValue = "jdbc")
public class MallOperateLogger implements OperateLogger {
    @Resource
    private SysLogMapper sysLogMapper;

    @Override
    @Async
    public void saveOperation(Operation operation) {
        SysLogPO po = new SysLogPO();
        BeanUtils.copyProperties(operation, po);
        //记录时间logTime
        po.setLogTime(LocalDateTime.now());
        try {
            // 记录服务器端主机名hostName、及服务器端hostIp
            InetAddress address = InetAddress.getLocalHost();
            po.setHostName(address.getHostName());
            po.setHostIp(address.getHostAddress());
        } catch (UnknownHostException e) {
            log.warn("日志记录主机信息时出现异常", e);
        }
        sysLogMapper.insert(po);
    }
}
