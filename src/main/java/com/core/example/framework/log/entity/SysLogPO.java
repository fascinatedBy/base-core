package com.core.example.framework.log.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 系统操作日志表
 * </p>
 *
 * @author wangwj
 * @since 2019-01-15
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "系统日志-视图对象-基本信息")
@TableName("sys_log")
public class SysLogPO {

    /**
     * 主键
     */
    @TableId(value = "log_id")
    @ApiModelProperty("日志ID")
    private String logId;

    /**
     * 用户ID
     */
    @ApiModelProperty("用户的ID")
    private String userId;

    /**
     * 用户帐号
     */
    @ApiModelProperty("用户的账号")
    private String account;

    /**
     * IP信息
     */
    @ApiModelProperty("用户的ip")
    private String userIp;

    /**
     * 被访问主机IP
     */
    @ApiModelProperty("服务器端ip")
    private String hostIp;

    /**
     * 被访问主机名
     */
    @ApiModelProperty("服务器端主机名")
    private String hostName;

    /**
     * 操作时间
     */
    @ApiModelProperty("操作时间")
    private LocalDateTime logTime;

    /**
     * 操作时长
     */
    @ApiModelProperty("耗时")
    private Integer costTime;

    /**
     * 访问路径
     */
    @ApiModelProperty("访问路径")
    private String url;

    /**
     * 权限名
     */
    @ApiModelProperty("权限名")
    private String permissionName;

    /**
     * 权限编码
     */
    @ApiModelProperty("权限编码")
    private String permissionCode;

    /**
     * 参数
     */
    @ApiModelProperty("参数")
    private String parameter;

    /**
     * 操作状态
     */
    @ApiModelProperty("操作状态-编码")
    private String logStatus;

    /**
     * 错误日志
     */
    @ApiModelProperty("异常信息")
    private String exception;

}
