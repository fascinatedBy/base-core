package com.core.example.bussiness.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.Base64Utils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * @description:
 * @author: chentao
 * @time: 2023/6/26 14:12
 */
@Slf4j
public class AesEncryptUtil {

    /** AES密钥长度，支持128、192、256 */
    private static final int AES_SECRET_KEY_LENGTH = 128;
    private static String getSha1SecretKey(String key) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(AES_SECRET_KEY_LENGTH);
            SecretKey secretKey = keyGenerator.generateKey();
            return Base64Utils.encodeToString(secretKey.getEncoded());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /** AES加密密钥 */
    public static final byte[] AES_SECRET_KEY_BYTES = Base64Utils.decodeFromString("Z5Z94FZs2y+SGjLhJnPYZQ==");
    /** SHA1加密密钥（用于增加加密的复杂度） */
    public static final String SHA1_SECRET_KEY = "b4SUNJDWWjTLAbAQ0ginuQ==";

    public static String aesEncrypt(String data) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding"); // 加密算法/工作模式/填充方式
            byte[] dataBytes = data.getBytes();
            cipher.init(Cipher.ENCRYPT_MODE,  new SecretKeySpec(AES_SECRET_KEY_BYTES, "AES"));
            byte[] result = cipher.doFinal(dataBytes);
            return Base64Utils.encodeToString(result);
        } catch (Exception e) {
            log.error("执行AesEncryptUtil.aesEncrypt失败：data={}，异常：{}", data, e);
            throw new EncryptException("执行aesDecrypt失败："+e.getMessage());
        }
    }

    public static String aesDecrypt(String encryptedDataBase64) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding"); // 加密算法/工作模式/填充方式
            byte[] dataBytes = Base64Utils.decodeFromString(encryptedDataBase64);
            cipher.init(Cipher.DECRYPT_MODE,  new SecretKeySpec(AES_SECRET_KEY_BYTES, "AES"));
            byte[] result = cipher.doFinal(dataBytes);
            return new String(result);
        } catch (Exception e) {
            log.error("执行AesEncryptUtil.aesDecrypt失败：data={}，异常：{}", encryptedDataBase64, e);
            return null;
        }
    }

    //使用SHA1加密
    public static String sha1Encrypt(String data) {
        return DigestUtils.sha1Hex(data + SHA1_SECRET_KEY);
    }


}
