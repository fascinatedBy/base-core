package com.core.example.bussiness.config;

/**
 * @description:
 * @author: chentao
 * @time: 2023/6/26 14:12
 */
public class EncryptException extends RuntimeException{
    public EncryptException(String s) {
        super(s);
    }
}
