package com.core.example.bussiness.config;

import org.springframework.util.DigestUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.io.IOException;
import java.security.SecureRandom;

/**
 * 加密方式需严格保密，同时请将平台的 app_key、app_secret妥善保管，严禁泄露，因
 * 泄露造成的安全问题由使用方自行承担。
 * 注意：签字拼接顺序必须按 appKey+secret+timestampStr+random执行 生成方式可参照 main方法
 *timestampStr：系统时间戳 获取方式 System.currentTimeMillis()+""
 * 随机数 建议6位数字 获取方式可参考 RandomUtil.randomNumbers(6);
 *
 * @author: ct
 * @time: 2023/6/20 20:04
 */
public class EncryptUtil {

    public static String key = "f81709802825bf66792e49f02029c8da488482ba";

    /**
     * 签字加密
     *
     * appKey：客户端id ，secret：密钥，timestampStr：系统时间戳，random：随机数，
     * 拼接顺序必须按 appKey+secret+timestampStr+random执行
     * timestampStr：系统时间戳 获取方式 System.currentTimeMillis()+""
     * random：随机数 建议6位数字 获取方式可参考 RandomUtil.randomNumbers(6);
     *
     * @param appKey
     * @param secret
     * @return
     */
    public static String signature(String appKey, String secret, String timestampStr, String random) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(appKey).append(secret).append(timestampStr).append(random);
        String asHex = DigestUtils.md5DigestAsHex(sb.toString().getBytes());
        return encrypt(asHex,key);
    }

    private final static String DES = "DES";

    /**
     * Description 根据键值进行加密
     * @param data
     * @param key  加密键byte数组
     * @return
     * @throws Exception
     */
    public static String encrypt(String data, String key) throws Exception {
        byte[] bt = encrypt(data.getBytes(), key.getBytes());
        String strs = parseByte2ToByte16Str(bt);
        return strs;
    }

    /**
     * Description 根据键值进行解密
     * @param data
     * @param key  加密键byte数组
     * @return
     * @throws IOException
     * @throws Exception
     */
    public static String decrypt(String data, String key) throws IOException, Exception {
        if (data == null)
            return null;
        byte[] buf = parseByte16StrToByte2(data);
        byte[] bt = decrypt(buf,key.getBytes());
        return new String(bt,"utf-8");
    }

    /**
     * Description 根据键值进行加密
     * @param data
     * @param key  加密键byte数组
     * @return
     * @throws Exception
     */
    private static byte[] encrypt(byte[] data, byte[] key) throws Exception {
        SecureRandom sr = new SecureRandom();
        DESKeySpec dks = new DESKeySpec(key);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance(DES);
        cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
        return cipher.doFinal(data);
    }


    /**
     * Description 根据键值进行解密
     * @param data
     * @param key  加密键byte数组
     * @return
     * @throws Exception
     */
    public static byte[] decrypt(byte[] data, byte[] key) throws Exception {
        SecureRandom sr = new SecureRandom();
        DESKeySpec dks = new DESKeySpec(key);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance(DES);
        cipher.init(Cipher.DECRYPT_MODE, securekey, sr);
        return cipher.doFinal(data);
    }

    /**
     * 将二进制转换成16进制
     *
     * @param buf
     * @return
     */
    public static String parseByte2ToByte16Str(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 将16进制转换为二进制
     *
     * @param hexStr
     * @return
     */
    public static byte[] parseByte16StrToByte2(String hexStr) {
        if (hexStr.length() < 1)
            return null;
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }


}
