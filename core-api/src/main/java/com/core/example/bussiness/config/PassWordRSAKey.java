package com.core.example.bussiness.config;

import lombok.Data;

/**
 * 存储RSA 的公钥和私钥
 */
@Data
public class PassWordRSAKey {
    private String publicKey;
    private String privateKey;
}
