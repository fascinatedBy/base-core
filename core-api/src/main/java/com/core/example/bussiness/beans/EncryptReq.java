package com.core.example.bussiness.beans;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description:
 * @author: chentao
 * @time: 2023/6/26 14:12
 */
@Data
public class EncryptReq<T> {

    /** 签名 */
    @NotBlank(message = "用户签名不能为空")
    private String sign;

    /** 加密请求数据 */
    @NotBlank(message = "参数不能为空")

    private String encryptedData;

    /** 原始请求数据（解密后回填到对象） */
    private T data;

    /** 请求的时间戳 */
    @NotNull(message = "时间戳不能为空")
    private Long timestamp;
}
