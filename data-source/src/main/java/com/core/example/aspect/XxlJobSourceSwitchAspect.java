package com.core.example.aspect;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @file: cn.chinaunicom.sdsi.aspect.XxlJobSourceSwitchAspect.java
 * @description: 数据源切换
 * @author: wz
 * @date: 2023-05-04 15:37
 * @version: 1.0
 * @_ 终日沉湎于过去之人，也终将被未来所弃
 **/
@Slf4j
@Aspect
@Component
public class XxlJobSourceSwitchAspect {

    @Pointcut(value = "@annotation(com.core.example.aspect.XxlJobSourceSwitch)")
    public void pointcut() {}

    @Before("pointcut() && @annotation(cc)")
    public void doBefore(JoinPoint joinPoint, XxlJobSourceSwitch cc){

    }

    //@After()

}
