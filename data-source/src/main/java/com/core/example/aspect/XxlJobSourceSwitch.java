package com.core.example.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @file: cn.chinaunicom.sdsi.aspect.DateSourceSwitch.java
 * @description: 数据源切换
 * @author: wz
 * @date: 2023-05-04 15:28
 * @version: 1.0
 * @_ 终日沉湎于过去之人，也终将被未来所弃
 **/
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface XxlJobSourceSwitch {

    String name() default "";

}
