package com.core.example.enums;
/**
 * B域 - M域 省码映射
 * @author wz
 * @date 2023/3/21 9:44
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 **/
public enum ProvinceRelationEnum {

    /**
     * 内蒙古
     */
    nm("10","15"),

    /**
     * 北京
     */
    bj("11","11"),

    /**
     * 天津
     */
    tj("13","12"),

    /**
     * 山东
     */
    sd("17","37"),

    /**
     * 河北
     */
    he("18","13"),

    /**
     * 山西
     */
    sx("19","14"),

    /**
     * 安徽
     */
    ah("30","34"),

    /**
     * 上海
     */
    sh("31","31"),

    /**
     * 江苏
     */
    js("34","32"),

    /**
     * 浙江
     */
    zj("36","33"),

    /**
     * 福建
     */
    fj("38","35"),

    /**
     * 海南
     */
    hi("50","46"),

    /**
     * 广东
     */
    gd("51","44"),

    /**
     * 广西
     */
    gx("59","45"),

    /**
     * 青海
     */
    qh("70","63"),

    /**
     * 湖北
     */
    hb("71","42"),

    /**
     * 湖南
     */
    hn("74","43"),

    /**
     * 江西
     */
    jx("75","36"),

    /**
     * 河南
     */
    ha("76","41"),

    /**
     * 西藏
     */
    xz("79","54"),

    /**
     * 四川
     */
    sc("81","51"),

    /**
     * 重庆
     */
    cq("83","50"),

    /**
     * 陕西
     */
    sn("84","61"),

    /**
     * 贵州
     */
    gz("85","52"),

    /**
     * 云南
     */
    yn("86","53"),

    /**
     *甘肃
     */
    gs("87","62"),

    /**
     * 宁夏
     */
    nx("88","64"),

    /**
     * 新疆
     */
    xj("89","65"),

    /**
     * 吉林
     */
    jl("90","22"),

    /**
     * 辽宁
     */
    ln("91","21"),

    /**
     * 黑龙江
     */
    hl("97","23");

    private final String code;
    private final String value;

    ProvinceRelationEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode(){
        return code;
    }

    public String getValue(){
        return value;
    }

    /**
     * 依据B域获取M域省码
     * @param code
     * @return
     * @author wz
     * @date 2023/3/21 10:09
     * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
     **/
    public static String getStockProvinceNum(String code) {
        for (ProvinceRelationEnum stockEnum : ProvinceRelationEnum.values()) {
            if (code.equals(stockEnum.getCode())) {
                return stockEnum.value;
            }
        }
        return null;
    }
}
