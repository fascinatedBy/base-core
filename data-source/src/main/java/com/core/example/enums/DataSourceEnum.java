package com.core.example.enums;

/**
 * 省份 - 数据源 对应关系 32个省份
 * @author wz
 * @date 2023/3/21 9:44
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 **/
public enum DataSourceEnum {

//    /**
//     * 集团总部
//     */
//    jtjc("10","master"),

    /**
     * 北京
     */
    bj("11","yxwz_db_three"),

    /**
     *天津
     */
    tj("12","yxwz_db_six"),

    /**
     *河北
     */
    he("13","yxwz_db_two"),

    /**
     *山西
     */
    sx("14","yxwz_db_nine"),

    /**
     *内蒙古
     */
    nm("15","yxwz_db_five"),

    /**
     *辽宁
     */
    ln("21","yxwz_db_nine"),

    /**
     *吉林
     */
    jl("22","yxwz_db_four"),

    /**
     *黑龙江
     */
    hl("23","yxwz_db_six"),

    /**
     *上海
     */
    sh("31","yxwz_db_three"),

    /**
     *江苏
     */
    js("32","yxwz_db_eight"),

    /**
     * 浙江
     */
    zj("33","yxwz_db_eight"),

    /**
     *安徽
     */
    ah("34","yxwz_db_eight"),

    /**
     *福建
     */
    fj("35","yxwz_db_five"),

    /**
     *江西
     */
    jx("36","yxwz_db_nine"),

    /**
     *山东
     */
    sd("37","yxwz_db_three"),

    /**
     *河南
     */
    ha("41","yxwz_db_seven"),

    /**
     *湖北
     */
    hb("42","yxwz_db_five"),

    /**
     *湖南
     */
    hn("43","yxwz_db_four"),

    /**
     *广东
     */
    gd("44","yxwz_db_one"),

    /**
     *广西
     */
    gx("45","yxwz_db_six"),

    /**
     *海南
     */
    hi("46","yxwz_db_nine"),

    /**
     *重庆
     */
    cq("50","yxwz_db_six"),

    /**
     *四川
     */
    sc("51","yxwz_db_four"),

    /**
     *贵州
     */
    gz("52","yxwz_db_seven"),

    /**
     *云南
     */
    yn("53","yxwz_db_seven"),

    /**
     *西藏
     */
    xz("54","yxwz_db_eight"),

    /**
     *陕西
     */
    sn("61","yxwz_db_eight"),

    /**
     *甘肃
     */
    gs("62","yxwz_db_five"),

    /**
     *青海
     */
    qh("63","yxwz_db_nine"),

    /**
     *宁夏
     */
    nx("64","yxwz_db_nine"),

    /**
     *新疆
     */
    xj("65","yxwz_db_four");

//    /**
//     *山东产业互联网
//     */
//    ch("69","master");


    private final String code;
    private final String value;

    DataSourceEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode(){
        return code;
    }

    public String getValue(){
        return value;
    }

    /**
     * 依据code获取value
     * @param code
     * @return
     * @author wz
     * @date 2023/3/21 10:09
     * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
     **/
    public static String getDataSource(String code) {
        for (DataSourceEnum sourceEnum : DataSourceEnum.values()) {
            if (code.equals(sourceEnum.getCode())) {
                return sourceEnum.value;
            }
        }
        return null;
    }
}
