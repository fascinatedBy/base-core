package com.core.example.interceptor;

import com.baomidou.dynamic.datasource.toolkit.DynamicDataSourceContextHolder;
import com.core.example.cloud.auth.MallUser;
import com.core.example.enums.DataSourceEnum;
import com.core.example.framework.utils.UnifastContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @file: cn.chinaunicom.sdsi.business.commontool.interceptor.SourceCutInterceptor.java
 * @description: 库存服务 -- 数据源切换
 * @author: wz
 * @date: 2023-03-21 09:26
 * @version: 1.0
 * @_ 终日沉湎于过去之人，也终将被未来所弃
 **/
@Slf4j
@Component
public class SourceCutInterceptor implements HandlerInterceptor {

    @Value("${spring.datasource.webcutsource:}")
    private String webcutsource;
    @Resource
    private UnifastContext unifastContext;
    /**
     * 数据源分源标识
     */
    private static final String SOURCE_KEY = "provinceNum";

    /**
     * 处理器运行之前执行
     * @param request
     * @param response
     * @param handler
     * @return 返回值为false时 终止当前拦截器后面配置的拦截器的运行
     * @author wz
     * @date 2023/3/21 9:31
     * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
     **/
    @Override
    public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler) {

        //依据配置文件中的 web数据源切换标识 判断是否执行拦截: 默认拦截
        if(!"true".equals(webcutsource)){
            return true;
        }

        // 获取请求头中的 数据源分源标识
        String provinceNum = request.getHeader(SOURCE_KEY);
        //请求头中不存在source key
        if(StringUtils.isBlank(provinceNum)){
            //获取当前人信息
            MallUser user = unifastContext.getUser();
            if(null == user || StringUtils.isBlank(user.getProvinceNum()) || "null".equals(user.getProvinceNum())){
                return true;
            }
            provinceNum = user.getProvinceNum();
        }
        String dataSourceKey = DataSourceEnum.getDataSource(provinceNum);

        log.info("{} 使用数据源: {}",request.getRequestURI(),dataSourceKey);
        if(StringUtils.isNotBlank(dataSourceKey)){
            DynamicDataSourceContextHolder.push(dataSourceKey);
        }
        return true;
    }

    /**
     * 处理器运行之后执行
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @return 无
     * @author wz
     * @date 2023/3/21 9:30
     * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
     **/
    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response,
            Object handler, ModelAndView modelAndView) {

    }

    /**
     * 所拦截器的后置执行全部结束后，执行该操作
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @return 无
     * @author wz
     * @date 2023/3/21 9:29
     * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
     **/
    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response,
            Object handler, Exception ex) {
        //清除当前线程源
        DynamicDataSourceContextHolder.clear();
    }
}
