package com.core.example.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @file: cn.chinaunicom.sdsi.config.WebConfigurer
 * @description: WebMvc容器注册工具类
 * @author: Menghh
 * @date: 2022-10-13
 * @version: V1.0
 * @update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 */
@Configuration
public class WebConfigurer implements WebMvcConfigurer {

    /**
     * 注入自定义拦截器实例
     */
    @Resource
    private SourceCutInterceptor myInterceptor;

    /**
     * 这个方法是用来配置静态资源的，比如html，js，css，等等
     * @author  menghh
     * @date    2022-10-13 15:22
     * @param	registry	registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    }

    /**
     * 这个方法用来注册拦截器，我们自己写好的拦截器需要通过这里添加注册才能生效
     * @author  CaiJW
     * @date    2022-10-13 15:23
     * @param	registry	registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // addPathPatterns("/**") 表示拦截所有的请求，
        // excludePathPatterns("/login", "/register") 表示除了登陆与注册之外，因为登陆注册不需要登陆也可以访问
        registry.addInterceptor(myInterceptor).addPathPatterns("/**").excludePathPatterns("/login", "/register");
    }
}