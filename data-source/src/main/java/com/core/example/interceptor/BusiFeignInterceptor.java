package com.core.example.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @file: cn.chinaunicom.sdsi.common.interceptor.BusiFeignInterceptor.java
 * @description: 传递请求头信息
 * @author: wz
 * @date: 2023-03-28 14:50
 * @version: 1.0
 * @_ 终日沉湎于过去之人，也终将被未来所弃
 **/
@Component
public class BusiFeignInterceptor implements RequestInterceptor {


    @Override
    public void apply(RequestTemplate template) {
        // 从上下文获取请求对象
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (null != requestAttributes) {
            // 获取request请求对象
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            // 获取Attribute中的省码信息
            String provinceNum = (String) request.getAttribute("provinceNum");
            // 因为通过feign之后在本项目中Attribute值和header值都没了，这里set通过feign可以获取到
            template.header("provinceNum", provinceNum);
        }
    }
}
